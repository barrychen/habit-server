/**
 * 获取分页信息
 * 
 * start 当前页数 total 总条数
 */
function getPageList(start, total) {
	var pageInfo = '';
	var pageCount = Math.ceil(total / pageSize);
	if (start > 1) {
		pageInfo += '<li class="prev"><a style="height:29.2px;" href="javascript:void(0)" onClick="getData('
				+ (start - 1)
				+ ')"><i class="icon-double-angle-left"></i></a></li>';
	} else {
		pageInfo += '<li class="prev disabled"><a style="height:29.2px;" href="javascript:void(0)"><i class="icon-double-angle-left"></i></a></li>';
	}
	var index = 1;
	if (start - pagePadding > 0) {
		index = start - pagePadding;
	} else {
		index = 1;
	}
	var end = 0;
	if (start + pagePadding < pageCount) {
		end = start + pagePadding;
	} else {
		end = pageCount;
	}
	for (var i = index; i <= end; i++) {
		pageInfo += '<li';
		if (i == start) {
			pageInfo += ' class="active"';
		}
		pageInfo += '><a href="javascript:void(0)" onClick="getData(' + i
				+ ')">' + i + '</a></li>';
	}
	if (start < pageCount) {
		pageInfo += '<li class="next"><a href="javascript:void(0)" onClick="getData('
				+ (start + 1)
				+ ')" style="height:29.2px;"><i class="icon-double-angle-right"></i></a></li>';
	} else {
		pageInfo += '<li class="next disabled"><a href="javascript:void(0)"   style="height:29.2px;"><i class="icon-double-angle-right"></i></a></li>';
	}
	return pageInfo;
}
/**
 * 显示加载提示
 */
function showLoader() {
	$("#loading").show();
}

/**
 * 关闭加载提示
 */
function hideLoader() {
	$("#loading").hide();
}

/**
 * 显示操作系统
 * 
 * @param content
 *            显示信息
 */
function showMessage(content) {
	$('#popover-content').text(content);
	$('.popover').slideDown();
	setTimeout(function() {
		$('.popover').slideUp();
	}, 2000);
}

/**
 * 展开/收缩 搜索框
 * 
 * @param but
 */
function expand(but) {
	var div = $(but).parent().next();
	if (div.is(":visible")) {
		div.slideUp("fast");
	} else {
		div.slideDown("fast");
	}
}

/**
 * 切割字符串 中文处理
 * 
 * @param content
 *            切割的字符串
 * @param maxlen
 *            长度
 * @returns
 */
function cut_str(content, maxlen) {
	var char_length = 0;
	for (var i = 0; i < content.length; i++) {
		var son_str = content.charAt(i);
		encodeURI(son_str).length > 2 ? char_length += 1 : char_length += 0.5;
		// 如果长度大于给定的n个字，就进行截取
		if (char_length >= maxlen) {

			var sub_len = char_length == maxlen ? i + 1 : i;
			var tmp = content.substr(0, sub_len);
			return tmp + "...";
		}
	}
	return content;
}

/**
 * 解析时间撮 -> 2015-03-05
 * 
 * @param d
 * @returns {String}
 */
function parseDate(d) {
	var date = new Date(d);
	var y = date.getFullYear();
	var m = date.getMonth() + 1;
	var d = date.getDate();
	if (m <= 9)
		m = "0" + m;
	if (d <= 9)
		d = "0" + d;
	var cdate = y + "-" + m + "-" + d;
	return cdate;
}

function parseDateTime(d) {
	var date = new Date(d);
	var y = date.getFullYear();
	var m = date.getMonth() + 1;
	var d = date.getDate();
	if (m <= 9)
		m = "0" + m;
	if (d <= 9)
		d = "0" + d;
	var cdate = y + "-" + m + "-" + d;
	return cdate + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
}

/**
 * 区域城市的选项绑定
 * 
 * @param city
 * @param index
 *            省份
 * @param selected
 *            市选择值
 */
function bindCity(city, index, selected) {
	$.ajax({
		url : contextPath + '/city/listByProvice.do',
		type : "POST",
		dataType : 'json',
		data : {
			provinceId : index,
			cityState : 1
		},
		beforeSend : function() {
			showLoader();
		},
		complete : function() {
			hideLoader();
		},
		success : function(rs, textStatus, jqXHR) {
			if (rs.result == "success") {
				var data = rs.data;
				var options = "";
				for (var i = 0; i < data.length; i++) {
					options += '<option value="' + data[i].cityId + '"';
					if (selected == data[i].cityId) {
						options += 'selected';
					}
					options += '>' + data[i].cityName + '</option>';
				}
				city.empty();
				city.append(options);

			} else {
				showMessage("获取数据失败,请稍后再试");
			}

		},
		error : function() {
			$("#loading").hide();
			showMessage("请稍后再试");
		}
	});
}

/**
 * 区域省份绑定 省市联动
 * 
 * @param province
 *            省份控件
 * @param city
 *            城市控件
 * @param selected
 *            省份选择值
 */
function bindProvince(province, city, selected) {
	$.ajax({
		url : contextPath + '/province/list.do',
		type : "POST",
		dataType : 'json',
		beforeSend : function() {
			showLoader();
		},
		complete : function() {
			hideLoader();
		},
		success : function(rs, textStatus, jqXHR) {
			if (rs.result == "success") {
				var data = rs.data;
				var options = "";
				for (var i = 0; i < data.length; i++) {
					options += '<option value="' + data[i].provId + '"';
					if (selected == data[i].provId) {
						options += 'selected';
					}
					options += '>' + data[i].provName + '</option>';
				}
				province.append(options);
				province.change(function() {
					var index = $(this).val();
					if (index > -1) {
						bindCity(city, index, -1);
					}
				});
			} else {
				showMessage("获取数据失败,请稍后再试");
			}

		},
		error : function() {
			$("#loading").hide();
			showMessage("请稍后再试");
		}
	});
}