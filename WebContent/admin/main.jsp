<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cyou.fz.common.web.WebContext" %>
<%@page import="com.cyou.fz.pay.business.po.AdminUserPO" %>
<%@page import="com.cyou.fz.pay.business.constant.SessionKeyConstant" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%
String contextPath = request.getContextPath();
AdminUserPO adminUser = (AdminUserPO)WebContext.getSessionAttribute(SessionKeyConstant.ADMIN_LOGIN_USER);
if(adminUser == null){
	// 未登录
	response.sendRedirect(contextPath + "/login.jsp?state=" + "session-invalid");
	return;
}
String realName =(String) WebContext.getSessionAttribute(SessionKeyConstant.ADMIN_LOGIN_USER_ACCOUNT);
Long adminId = adminUser.getId();
%>
<html  xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="assets/css/font-awesome.min.css" />

		<!--[if IE 7]>
		  <link rel="stylesheet" href="assets/css/font-awesome-ie7.min.css" />
		<![endif]-->

		<!-- page specific plugin styles -->

		<!-- fonts -->


		<!-- ace styles -->

		<link rel="stylesheet" href="assets/css/ace.min.css" />
		<link rel="stylesheet" href="assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="assets/css/ace-skins.min.css" />

		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->

		<script src="assets/js/ace-extra.min.js"></script>

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

		<!--[if lt IE 9]>
		<script src="assets/js/html5shiv.js"></script>
		<script src="assets/js/respond.min.js"></script>
		<![endif]-->
        <script type="text/javascript">      
        var contextPath='<%=contextPath%>';
	</script>
	<style>
		
	</style>
    </head>
<body>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-5" style="height:335px;padding-left:20px;" >
					<div id="playerReport" style="height:335px;"></div>
				</div>
				<div class="col-md-5" style="height:335px;padding-left:5px;">
					<div id="teamReport" style="height:335px;"></div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>"+"<"+"script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
		<script type="text/javascript">
		 window.jQuery || document.write("<script src='assets/js/jquery-1.10.2.min.js'>"+"<"+"script>");
		</script>
		<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"script>");
		</script>
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/js/typeahead-bs2.min.js"></script>

		<!-- page specific plugin scripts -->

		<!--[if lte IE 8]>
		  <script src="assets/js/excanvas.min.js"></script>
		<![endif]-->

		<script src="assets/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="assets/js/jquery.slimscroll.min.js"></script>
		<script src="assets/js/jquery.easy-pie-chart.min.js"></script>
		<script src="assets/js/jquery.sparkline.min.js"></script>
		<script src="assets/js/flot/jquery.flot.min.js"></script>
		<script src="assets/js/flot/jquery.flot.pie.min.js"></script>
		<script src="assets/js/flot/jquery.flot.resize.min.js"></script>
		<script src="assets/js/flot/jquery.flot.time.js"></script>
		<script src="js/common.js"></script>
		<!-- ace scripts -->

		<script src="assets/js/ace-elements.min.js"></script>
		<script src="assets/js/ace.min.js"></script>

		<!-- inline scripts related to this page -->

		<script type="text/javascript">
			var height = $(window).height();
			$('body').height(height);
			$.ajax({
				url : contextPath + '/admin/report/playerReport.do',
				type : "POST",
				dataType : 'json',
				beforeSend : function() {
					showLoader();
				},
				complete : function() {
					hideLoader();
				},
				success : function(rs, textStatus, jqXHR) {
					if (rs.result == "success") {
						var data = rs.data;
						var plot_data = new Array();   
				        var plot_ticks = new Array();   
				        for (var i = 0 ; i < data.length ; i++) {    
				            plot_data.push([data[i].index, data[i].count]);   
				            plot_ticks.push([data[i].index, data[i].date]);   
				        }   
						var plot = $.plot($("#playerReport"),[{ label: "用户在近15天的增加情况",data:plot_data}],{ xaxis: { ticks:plot_ticks } });
					} else {
						showMessage("获取数据失败,请稍后再试");
					}

				},
				error : function() {
					showMessage("请稍后再试");
				}
			});	
			
			
			$.ajax({
				url : contextPath + '/admin/report/teamReport.do',
				type : "POST",
				dataType : 'json',
				beforeSend : function() {
					showLoader();
				},
				complete : function() {
					hideLoader();
				},
				success : function(rs, textStatus, jqXHR) {
					if (rs.result == "success") {
						var data = rs.data;
						var plot_data = new Array();   
				        var plot_ticks = new Array();   
				        for (var i = 0 ; i < data.length ; i++) {    
				            plot_data.push([data[i].index, data[i].count]);   
				            plot_ticks.push([data[i].index, data[i].date]);   
				        }   
						var plot = $.plot($("#teamReport"),[{ label: "球队在近15天的增加情况",data:plot_data}],{ xaxis: { ticks:plot_ticks } });
					} else {
						showMessage("获取数据失败,请稍后再试");
					}

				},
				error : function() {
					showMessage("请稍后再试");
				}
			});	
			
		</script>

				
		
    </body>
</html>