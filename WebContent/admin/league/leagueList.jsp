﻿<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cyou.fz.common.web.WebContext"%>
<%@page import="com.cyou.fz.pay.business.po.AdminUserPO"%>
<%@page import="com.cyou.fz.pay.business.constant.SessionKeyConstant"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
	String contextPath = request.getContextPath();
	AdminUserPO adminUser = (AdminUserPO) WebContext
			.getSessionAttribute(SessionKeyConstant.ADMIN_LOGIN_USER);
	if(adminUser == null){
		// 未登录
		response.sendRedirect(contextPath + "/login.jsp?state=" + "session-invalid");
		return;
	}
	String realName = (String) WebContext
			.getSessionAttribute(SessionKeyConstant.ADMIN_LOGIN_USER_ACCOUNT);
	Long adminId = adminUser.getId();
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>赛事管理</title>
<link href="../css/base.css" rel="stylesheet" />
<link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="../assets/css/font-awesome.min.css" />

<!--[if IE 7]>
		  <link rel="stylesheet" href="../assets/css/font-awesome-ie7.min.css" />
		<![endif]-->



<link rel="stylesheet" href="../assets/css/ace.min.css" />
<link rel="stylesheet" href="../assets/css/ace-rtl.min.css" />
<link rel="stylesheet" href="../assets/css/ace-skins.min.css" />

<!--[if lte IE 8]>
		  <link rel="stylesheet" href="../assets/css/ace-ie.min.css" />
		<![endif]-->



<script src="../assets/js/ace-extra.min.js"></script>


<!--[if lt IE 9]>
		<script src="../assets/js/html5shiv.js"></script>
		<script src="../assets/js/respond.min.js"></script>
		<![endif]-->
<script type="text/javascript">  
       		var contextPath='<%=contextPath%>';
</script>
<link rel="stylesheet"
	href="../assets/css/jquery-ui-1.10.3.custom.min.css" />
<link rel="stylesheet" href="../assets/css/chosen.css" />
<style type="">
	.cutshow{text-decoration: none;}
	.cutshow:hover{text-decoration: none}
</style>
</head>
<body style="background-color: #fff;">
	<div class="breadcrumbs" id="breadcrumbs">
		<script type="text/javascript">
			try {
				ace.settings.check('breadcrumbs', 'fixed')
			} catch (e) {
			}
		</script>
		<ul class="breadcrumb">
			<li><i class="icon-home home-icon"></i> <a href="../index.jsp">首页</a>
			</li>
			<li class="active">赛事管理</li>
		</ul>
		<!-- .breadcrumb -->
	</div>
	<div class="page-content">
		<div class="page-header">
			<h1>
				赛事管理 <small> <i class="icon-double-angle-right"> 赛事管理 </i>
				</small>
			</h1>
		</div>
		<!-- /.page-header -->
		<div class="row" style="height: 100%">

			<div class="col-xs-12">
				<div class="panel panel-default">
					<div class="panel-heading" style="padding: 0px;">
						<span class="icon"> <i class="icon-search"></i>
						</span>
						<h5>赛事搜索</h5>
						<button type="button" onClick="expand(this)"
							class="func_btn pull-right">
							<i class="icon-refresh"></i> 展开/关闭
						</button>
					</div>
					<div class="panel-body" style="background-color: #F9F9F9;">
						<form class="form-inline" name="myForm" style="margin: 0px">
							 <div class="form-group">
								<label>省份：</label>
								<select	id="province" class="form-control" style="width:140px;border-radius: 4px !important;">
									<option value="-1" selected>
										请选择省份
									</option>
									
								</select>
							  </div>
							   <div class="form-group">
								<label>城市：</label>
								<select	id="city" class="form-control" style="width:140px;border-radius: 4px !important;">
									
								</select>
							  </div>
							<div class="form-group">
								<button type="button" class="func_btn" id="Search"
									onclick="search()">
									<i class="icon-search"></i> 搜 索
								</button>
							</div>
						</form>

					</div>
				</div>
				<div style="margin-bottom: 10px;">
					<button type="button" class="btn btn-primary btn-sm" id="add">
						<i class="icon-plus"></i>添加赛事
					</button>
				</div>
				<div class="table-responsive">
					<input type="hidden" value="1" id="start" />
					<input type="hidden" value="" id="property" />
					<table id="sample-table-2"
						class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th class="center"><label> <input type="checkbox"
										class="ace" /> <span class="lbl"></span>
								</label></th>
								<th>联赛名</th>
								<th>开始时间</th>
								<th>结束时间</th>
								<th>区域</th>
								<th>广告图片</th>
								<th>参赛数目</th>
								<th>是否显示</th>
								<th>操作</th>
							</tr>
						</thead>

						<tbody id="data-list">

						</tbody>
					</table>
				</div>
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
		<div class="row">
			<div class="col-sm-6">
				<div id="sample-table-2_info" class="dataTables_info">
					当前 <span id="current"></span> 到 <span id="page"></span> 总共 <span
						id="total"></span> 条
				</div>
			</div>
			<div class="col-sm-6">
				<div class="dataTables_paginate paging_bootstrap">
					<ul class="pagination pagination-sm" id="page-list">

					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="popover"
		style="z-index: 9999; width: 150px; position: absolute; left: 45%;">
		<div class="arrow"></div>
		<div class="popover-inner">
			<h3 class="popover-title" >操作提示</h3>
			<div class="popover-content" id="popover-content">
				<p>操作成功</p>
			</div>
		</div>
	</div>

	<div id="addAdv" style="display:none;z-index: 9999; width: 400px; position: absolute; left: 35%;top:25%;border:1px solid #ccc;border-radius: 4px !important;">
		<div class="arrow"></div>
		<div class="popover-inner" style="">
			<h3 class="popover-title" id="">添加赛事宣传海报</h3>
			<div class="popover-content" id="" style="background-color:#F5F5F5;">
				<form class="form-horizontal" style="padding-top:10px;" method="post" id="leagueImageForm" enctype="multipart/form-data">
					<input type="hidden" value="" id="leagueId" name="leagueId" />
					<div class="form-group">
						<label for="" class="col-sm-3 control-label">描述：</label>
						<div class="col-sm-8">
							 <textarea rows="5" name="description" id="description" class="form-control"></textarea>
						</div>
						<span class="input_tip" style="line-height:30px;">*</span>
					</div>
					<div class="form-group">
						<label for="name" class="col-sm-3 control-label">海报：</label>
						<div class="col-sm-6">
							<input type="file"  name="image" id="image" />
						</div>
					</div>
					<div class="form-group">
						<label for="name" class="col-sm-3 control-label">顺序：</label>
						<div class="col-sm-6">
							<input type="text"  name="orderNum" id="orderNum" />
						</div>
					</div>
				</form>
			</div>
			<div class="popover-footer" style="background-color:#F5F5F5;overflow: hidden;">
				<button type="button" class="btn btn-primary" style="float: right;padding: 0px;margin: 0px 5px 5px 0px;" id="cancel">
						<i class="icon-ok"></i>取消
				</button>
				<button type="button" class="btn btn-danger" style="float: right;padding: 0px;margin: 0px 5px 5px 0px;" id="save">
						<i class="icon-edit"></i>保存
				</button>
				
			</div>
		</div>
	</div>
	
	<div id="loading"
		style="width: 32px; height: 32px; position: absolute; left: 40%; top: 40%; display: none;">
		<img src="<%=contextPath%>/admin/assets//css/images/loading.gif"
			width="32" height="32" />
	</div>
	<script type="text/javascript">
		if ("ontouchend" in document)
			document
					.write("<script src='../assets/js/jquery.mobile.custom.min.js'>"
							+ "<"+"/script>");
	</script>
	<script type="text/javascript">
		window.jQuery
				|| document
						.write("<script src='../assets/js/jquery-2.0.3.min.js'>"
								+ "<"+"script>");
	</script>

	<!-- <![endif]-->

	<!--[if IE]>
		<script type="text/javascript">
		window.jQuery || document.write("<script src='../assets/js/jquery-1.10.2.min.js'>"+"<"+"script>");
		</script>
		<![endif]-->
	<script type="text/javascript">
		if ("ontouchend" in document)
			document
					.write("<script src='../assets/js/jquery.mobile.custom.min.js'>"
							+ "<"+"script>");
	</script>
	<script src="../assets/js/bootstrap.min.js"></script>
	<script src="../assets/js/typeahead-bs2.min.js"></script>

	<!-- page specific plugin scripts -->

	<!--[if lte IE 8]>
		  <script src="../assets/js/excanvas.min.js"></script>
		<![endif]-->

	<script src="../assets/js/jquery-ui-1.10.3.custom.min.js"></script>
	<script src="../assets/js/jquery.ui.touch-punch.min.js"></script>
	<script src="../assets/js/jquery.slimscroll.min.js"></script>
	<script src="../assets/js/jquery.easy-pie-chart.min.js"></script>
	<script src="../assets/js/jquery.sparkline.min.js"></script>
	<script src="../assets/js/fuelux/fuelux.spinner.min.js"></script>
	<!-- ace scripts -->
	<script src="../assets/js/ace-elements.min.js"></script>
	<script src="../assets/js/ace.min.js"></script>
	<!-- inline scripts related to this page -->
	<script src="../assets/js/jquery.dataTables.bootstrap.js"></script>
	<script src="../js/config.js"></script>
	<script src="../js/common.js"></script>
	<script src="../js/city.js"></script>
	<script src="../assets/js/jquery-form.js"></script>
	<script type="text/javascript">
		$('#orderNum').ace_spinner({value:1,min:1,max:200,step:1, btn_up_class:'btn-info' , btn_down_class:'btn-info'})
		.on('change', function(){
		});
		init();
		function init() {
			bindProvince($("#province"),$("#city"));
			$("#city").prepend('<option value="-1">请选择城市</option>');
			getData(1);
		}
		function getData(start) {
			$("#start").val(start);
			$.ajax({
				url : contextPath + '/admin/league/list.do' + ($("#property").val() == "" ? "" : '?' + $("#property").val()),
				type : "POST",
				dataType : 'json',
				data : {
					pageNo : (start - 1) * pageSize,
					pageSize : pageSize
				},
				beforeSend : function() {
					showLoader();
				},
				complete : function() {
					hideLoader();
				},
				success : function(rs, textStatus, jqXHR) {
					if (rs.result == "success") {
						var data = rs.data;
						var total = data.totalCount;
						var root = data.dataList;
						var dataList = $("#data-list");
						dataList.empty();
						$("#total").text(total);
						$("#current").text((start - 1) * pageSize + 1);
						if (root.length > 0) {
							$("#page").text(
									(start - 1) * pageSize + root.length);
							parseData(root, total, start);
						} else {
							showMessage("无数据");
						}
						
					} else {
						showMessage("获取数据失败,请稍后再试");
					}

				},
				error : function() {
					$("#loading").hide();
					showMessage("请稍后再试");
				}
			});
		}

		var isShow = [ '显示', '隐藏' ];
		function parseData(data, total, start) {
			var dataList = $("#data-list");
			var tr = "";
			for (var i = 0; i < data.length; i++) {
				tr += '<tr><td class="center"><label><input type="checkbox" value="'+data[i].id+'" class="ace" /><span class="lbl"></span></label></td>';
				tr += '<td>' + data[i].title + '</td>';
				tr += '<td>' + parseDate(data[i].startDate) + '</td>';
				tr += '<td>' + parseDate(data[i].endDate) + '</td>';
				tr += '<td>' + data[i].area + '</td>';
				tr += '<td><a href="'+contextPath + '/' + data[i].adv+'" target="_black"><img src="'+contextPath + '/' + data[i].adv+'" width="60" height="35" title="点击查看大图"></a></td>';
				tr += '<td>' + data[i].teamCount + '支</td>';
				tr += '<td>' + isShow[data[i].isShow] + '</td>';
				tr += '<td><div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">';
				tr += '<a class="blue" href="leagueEdit.jsp?id='+data[i].id+'" title="修改"><i class="icon-edit bigger-130"></i></a>&nbsp;';
				if(data[i].isShow == 0){
					tr += '<label><input name="switch-field-1" class="ace ace-switch unShow" checked="checked"  type="checkbox" style="margin-top:5px;" /><span class="lbl"></span></label>'
					// tr += '<a class="pink unShow" href="javascript:void(0)" title="隐藏"><i class="icon-arrow-down bigger-130"></i></a>&nbsp;';
				}else{
					tr += '<label><input name="switch-field-1" class="ace ace-switch isShow" type="checkbox" style="margin-top:5px;" /><span class="lbl"></span></label>';
					// tr += '<a class="pink isShow" href="javascript:void(0)" title="显示"><i class="icon-arrow-up bigger-130"></i></a>&nbsp;';
				}
				tr += '<a class="yellow" href="leagueInfo.jsp?id='+data[i].id+'" title="详细信息"><i class="icon-search bigger-130"></i></a>&nbsp;';
				tr += '<a class="green match" href="leagueMatch.jsp?id='+data[i].id+'" title="赛事匹配"><i class="icon-cog bigger-130"></i></a>&nbsp;';
				//tr += '<a class="red addAdv" href="javascript:void(0)" title="上传赛事海报"><i class="icon-upload bigger-130"></i></a>&nbsp;';
				tr += '<a class="red" href="leagueAdv.jsp?id='+data[i].id+'" title="上传赛事海报"><i class="icon-upload bigger-130"></i></a>&nbsp;';
				tr += '<a class="red" href="../push/pushLeague.jsp?id='+data[i].id+'" title="赛事推送"><i class="icon-cloud bigger-130"></i></a>&nbsp;';
				tr += '</div></td>';
				tr += '</tr>';
			}
			dataList.append(tr);
			//分页
			var pageList = $("#page-list");
			pageList.empty();
			pageList.append(getPageList(start, total));
			$(".unShow").bind("click",function(){
				var tr = $(this).parent().parent().parent();
				var id = tr.find("input").val();
				changeShow(id,1);
			});
			$(".isShow").bind("click",function(){
				var tr = $(this).parent().parent().parent();
				var id = tr.find("input").val();
				changeShow(id,0);
			});
			$(".addAdv").bind("click",function(){
				var tr = $(this).parent().parent().parent();
				var id = tr.find("input").val();
				$("#leagueId").val(id);
				$("#addAdv").show();
			});
			
		}
		
		$("#save").bind("click",function(){
			var description = $("#description");
			var image = $("#image");
			if(description.val() == ""){
				showMessage("请输入描述!!!");
				return;
			}
			if(image.val() == ""){
				showMessage("请选择图片!!!");
				return;
			}
			$("#leagueImageForm").ajaxSubmit({
				type: "POST",
				url: contextPath + '/admin/league/addAdv.do',
				dataType:'json',
				success: function (data) {
					if(data.result == "success"){
						showMessage("添加成功!!!");
						description.val("");
						image.val("");
						$("#oderNum").val("0");
						$("#addAdv").hide();
					}else{
						showMessage("添加失败");
					}
					$("#loading").hide();
				},
				error: function (msg) {
					$("#loading").hide();
					showMessage("添加失败");
				}
			});
		});
		
		$("#cancel").bind("click",function(){
			$("#description").val("");
			$("#image").val("");
			$("#oderNum").val("0");
			$("#addAdv").hide();
		});
		
		function search() {
			var property = "";
			var province = $("#province").val();
			if(province != -1 ) {
				property += "areaProvince=" + province + ",";
			}
			var city = $("#city").val();
			if(city != -1){
				property += 'areaCity=' + city + ",";
			}
			if(property != ""){
				property = property.substring(0,property.length -1 );
				property = property.replace(/,/,"&");
				$("#property").val(property);
			}else{
				$("#property").val("");
			}
			getData(1);
		}

		
		$("#add").bind("click",function(){
			window.location.href='leagueEdit.jsp';
		});
		
		function changeShow(id,isShow){
			$.ajax({
				url : contextPath + '/admin/league/changeShow.do?id='+id+'&isShow=' + isShow,
				type : "POST",
				dataType : 'json',
				beforeSend : function() {
					showLoader();
				},
				complete : function() {
					hideLoader();
				},
				success : function(rs, textStatus, jqXHR) {
					if (rs.result == "success") {
						showMessage("操作成功");
						getData($("#start").val());
					} else {
						showMessage("获取数据失败,请稍后再试");
					}

				},
				error : function() {
					$("#loading").hide();
					showMessage("请稍后再试");
				}
			});
		}
		
		
		
		$("#province").change(function(){
			if($("#city").find("option[value=-1]").length == 0){
				$("#city").prepend('<option value="-1">请选择城市</option>');
				$("#city").val(-1);
			}
		});
	</script>
</body>
</html>