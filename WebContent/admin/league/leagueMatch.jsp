<%@ page language="java" pageEncoding="UTF-8"%> 
<%@page import="com.cyou.fz.common.web.WebContext"%>
<%@page import="com.cyou.fz.pay.business.po.AdminUserPO"%>
<%@page import="com.cyou.fz.pay.business.constant.SessionKeyConstant"%>
<%
String contextPath = request.getContextPath();
AdminUserPO adminUser = (AdminUserPO) WebContext
.getSessionAttribute(SessionKeyConstant.ADMIN_LOGIN_USER);
if(adminUser == null){
	// 未登录
	response.sendRedirect(contextPath + "/login.jsp?state=" + "session-invalid");
	return;
}
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+contextPath+"/";
String leagueId = request.getParameter("id");
%>
<%@page import="java.util.UUID"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
		<link href="../css/base.css" rel="stylesheet" />
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="../assets/css/font-awesome.min.css" />

		<!--[if IE 7]>
		  <link rel="stylesheet" href="../assets/css/font-awesome-ie7.min.css" />
		<![endif]-->

		

		<link rel="stylesheet" href="../assets/css/ace.min.css" />
		<link rel="stylesheet" href="../assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="../assets/css/ace-skins.min.css" />
		<link rel="stylesheet" href="../assets/css/datepicker.css" />
		<link rel="stylesheet" href="../css/base.css" />
		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="../assets/css/ace-ie.min.css" />
		<![endif]-->

		

		<script src="../assets/js/ace-extra.min.js"></script>


		<!--[if lt IE 9]>
		<script src="../assets/js/html5shiv.js"></script>
		<script src="../assets/js/respond.min.js"></script>
		<![endif]-->
       	<script type="text/javascript">  
       		var contextPath='<%=contextPath%>';
       		var leagueId = '<%=leagueId%>';
		</script>
		<link rel="stylesheet" href="../assets/css/jquery-ui-1.10.3.custom.min.css" />
		<link rel="stylesheet" href="../assets/css/chosen.css" />
	</head>
	<body style="padding:0;margin:0;background-color:#fff;">
		<div class="breadcrumbs" id="breadcrumbs">
			<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="leagueList.jsp">赛事管理</a>
							</li>
							<li class="active">赛事匹配</li>
						</ul>
		</div>
		
		<div class="container-fluid" style="padding:0 20px;font-size:12px;margin-top:5px;">
			<div class="row-fluid">
				<div class="span12">
					<div class="panel panel-default">
					  <div class="panel-heading">
							<span class="icon">
								<i class="icon-th"></i>
							</span>
							<h5>赛事匹配</h5>
					  </div>
					  <div class="panel-body" style="padding:20px;background-color:#F9F9F9;">
						  <ul class="nav nav-tabs" role="tablist" id="myTab">
						  </ul>
							
							<div class="tab-content" id="tabContent">
							</div>
					  </div>
					   <div class="panel-footer">
							<button type="button" class="btn btn-primary" style="padding:0px;" id="saveMatch">
								<i class="icon-ok"></i>生成赛事名单
							</button>
							<button type="button" class="btn btn-primary" style="padding:0px;" id="randomMatch">
								<i class="icon-ok"></i>随机
							</button>
					  </div>
					</div>
				</div>
				
			</div>
		</div>
		
		<div class="container-fluid" style="padding:0 20px;font-size:12px;margin-top:5px;">
			<div class="row-fluid">
				<div class="span12">
					<div class="panel panel-default">
					  <div class="panel-heading">
							<span class="icon">
								<i class="icon-th"></i>
							</span>
							<h5>赛事信息</h5>
					  </div>
					  <div class="panel-body" style="padding:0px;background-color:#F9F9F9;">
							<form class="form-horizontal" style="padding:10px 0px 0px 0px;" method="post" id="leagueForm" enctype="multipart/form-data">
							  <input type="hidden" value="" id="id" name="id" />
							  <div class="form-group">
								<label for="" class="col-sm-1 control-label">名称：</label>
								<div class="col-sm-2">
								  <input id="title" class="form-control" maxlength="50" type="text" name="title" value="" readOnly="readOnly"/>
								</div>
								
								<label for="" class="col-sm-1 control-label">联赛区域：</label>
								<div class="col-sm-2">
									<input id="area" class="form-control" maxlength="50" type="text" name="title" value="" readOnly="readOnly"/>
								</div>
								
								
									<label for="" class="col-sm-1 control-label">开始时间：</label>
									<div class="col-sm-2" style="padding:0px;">
										<div class="input-group col-xs-12 col-sm-12">
											<input class="form-control col-xs-12 col-sm-12"  type="text" name="startDate" id="startDate" readOnly="readOnly" />
										</div>
									</div>
									<label for="" class="col-sm-1 control-label">结束时间：</label>
									<div class="col-sm-2" style="padding:0px;">
										<div class="input-group col-xs-12 col-sm-12">
											<input class="form-control col-xs-12 col-sm-12"  type="text" name="endDate" id="endDate" readOnly="readOnly" />
										</div>
									</div>
							  </div>
							  <div class="form-group">
								<label for="" class="col-sm-1 control-label">规则：</label>
								<div class="col-sm-7">
								  <textarea rows="5" name="rule" id="rule" class="form-control" readOnly="readOnly"></textarea>
								</div>
							  </div>
							  
							  <div class="form-group">
								<label for="name" class="col-sm-1 control-label">广告图片：</label>
								<div class="col-sm-8">
									<img id="advImage" src="" width="200" height="100" />
								</div>
							  </div>
							</form>
					  </div>
					</div>
				</div>
			</div>
		</div>
		
<div class="popover" style="z-index:9999;width:150px;position:absolute;left:45%;"><div class="arrow"></div><div class="popover-inner"><h3 class="popover-title">操作提示</h3><div class="popover-content" id="popover-content"><p>操作成功</p></div></div></div>

<div id="loading" style="width:32px;height:32px;position:absolute;left:40%;top:40%;display:none;">
	<img src="<%=contextPath%>/admin/assets//css/images/loading.gif" width="32" height="32" />
</div>
		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='../assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script type="text/javascript">
			window.jQuery || document.write("<script src='../assets/js/jquery-2.0.3.min.js'>"+"<"+"script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
		<script type="text/javascript">
		window.jQuery || document.write("<script src='../assets/js/jquery-1.10.2.min.js'>"+"<"+"script>");
		</script>
		<![endif]-->
		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='../assets/js/jquery.mobile.custom.min.js'>"+"<"+"script>");
		</script>
		<script src="../assets/js/bootstrap.min.js"></script>
		<script src="../assets/js/typeahead-bs2.min.js"></script>

		<!-- page specific plugin scripts -->

		<!--[if lte IE 8]>
		  <script src="../assets/js/excanvas.min.js"></script>
		<![endif]-->

		<script src="../assets/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="../assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="../assets/js/jquery.slimscroll.min.js"></script>
		<script src="../assets/js/jquery.easy-pie-chart.min.js"></script>
		<script src="../assets/js/jquery.sparkline.min.js"></script>
		<script src="../assets/js/date-time/bootstrap-datepicker.min.js"></script>
		<script src="../assets/js/ace-elements.min.js"></script>
		<script src="../assets/js/ace.min.js"></script>
		<!-- inline scripts related to this page -->
		<script src="../assets/js/jquery.dataTables.bootstrap.js"></script>
		<script src="../assets/js/jquery-form.js"></script>
		<script src="../js/config.js"></script>
		<script src="../js/common.js"></script>
		<script src="../js/city.js"></script>
		
		<script>
			var teamList = new Array();
			var teamCount = 0;
			var gameCount = 0;
			var teamMatch = $("#teamMatch");
			var index = 0;
			var currentRound = 0;
			var totalRound = 0;
			var canContinue = 0;
			var dataList = new Array();
			var teamOptions = "";
			var onGameCount = 0;
			var teamIdList = new Array();
			init();
			
			function init(){
				getLeagueTeam();
				getTotalRount();
				getCurrentRound();
				addTabs();
				bindOldData();
				getOnGameTeam();
				showData();
				if(canContinue != 0){
					$("#randomMatch").prop("disabled","disabled");
					$("#saveMatch").prop("disabled","disabled");
				}else{
					match();
				}
				if(onGameCount  == 1){
					showMessage("比赛已经结束!!!");
					$("#randomMatch").prop("disabled","disabled");
					$("#saveMatch").prop("disabled","disabled");
				}
				//if(currentRound == totalRound){
				//	$("#randomMatch").prop("disabled","disabled");
				//	$("#saveMatch").prop("disabled","disabled");
				//}
				//teamMatched();
			}
			
			function showData(){
				if(dataList.length > 0){
					for(var i = 1 ; i <= currentRound;i++){
						var data = dataList[i];
						if (typeof(data) == "undefined") { 
							 continue;
						}  
						var round = $("#round" + i);
						var  content = "";
						var j = 0 ;
						for( ; j < data.length ; j++){
							if(data[j].status == 0){
								content += '<div id="game'+i+j+'" style="height:40px;">'+ '' +'第' + (j+1) + '场 ： ' ;
								content += '<span><select class="select'+i+'">'+teamOptions+'</select>';
								content += " VS ";
								content += '<select class="select'+i+'">'+teamOptions+'</select>';
								content += '&nbsp;&nbsp;<input type="hidden" value="'+data[j].id+'" name="gameId" /><button class="btn1 btn btn-default editMatch" type="button">修改</button></span>';
								canContinue ++ ;
								content += '<span>&nbsp;&nbsp;<input type="hidden" value="'+data[j].id+'" name="gameId" />'+data[j].guestTeamName+' <input name="guestTeamScore" type="text" style="width:60px;" /> : <input name="homeTeamScore" type="text" style="width:60px;" /> '+data[j].homeTeamName + ' &nbsp;&nbsp;<button class="btn1 btn btn-default statistics" type="button">保存</button></span>';
							}else if(data[j].status == 2){
								content += '<div id="game'+i+j+'" style="height:40px;">'+ '<input type="hidden" value="'+data[j].guestTeamId+'-' +data[j].homeTeamId +'" class="matchInfo'+i+'" />' +'第' + (j+1) + '场 ： ' ;
								content += data[j].guestTeamName + " VS " + data[j].homeTeamName ;
								if(data[j].guestTeamScore > data[j].homeTeamScore){
									content += '&nbsp;&nbsp;' + data[j].guestTeamName + "赢"  + " &nbsp;&nbsp; 比分 ： " +  data[j].guestTeamScore +' : '+ data[j].homeTeamScore;
								}else{
									content += '&nbsp;&nbsp;' + data[j].homeTeamName + "赢"  + " &nbsp;&nbsp; 比分 ： " +  data[j].guestTeamScore +' : '+ data[j].homeTeamScore;
								}
							}else{
								content += '<div id="game'+i+j+'" style="height:40px;">'+ '<input type="hidden" value="'+data[j].guestTeamId+'-' +data[j].homeTeamId +'" class="matchInfo'+i+'" />' +'第' + (j+1) + '场 ： ' ;
								content += data[j].guestTeamName + " VS " + data[j].homeTeamName ;
								content += '&nbsp;&nbsp; 平局  ' + " &nbsp;&nbsp; 比分 ： " +  data[j].guestTeamScore +' : '+ data[j].homeTeamScore;
							}
							content += '</div>';
						}
						if(canContinue != 0){
							currentRound--;
						}
						round.append(content);
						for(var j = 0 ;j < data.length ;j++){
							$("#game" + i + j).find("select:eq(0)").val(data[j].guestTeamId);
							$("#game" + i + j).find("select:eq(0)").prop("disabled","disabled");
							$("#game" + i + j).find("select:eq(1)").val(data[j].homeTeamId);
							$("#game" + i + j).find("select:eq(1)").prop("disabled","disabled");
						}
						
					}
					$(".editMatch").on("click",function(){
						if(onGameCount  <= 2){
							showMessage("只剩两支球队,无法修改!!!");
							return ;
						}
						if($(this).hasClass("editMatch")){
							$(this).text("保存");
							$(this).addClass("save").removeClass("editMatch");
							var span = $(this).parent();
							span.find("select:eq(0)").prop("disabled","");
							span.find("select:eq(1)").prop("disabled","");
						}else{
							var selected = "";
							var selects = $(".select" + (currentRound));
							var matchInfo = "";
							$(".matchInfo" + currentRound).each(function(){
								var input = $(this);
								matchInfo += input.val() + ",";
							});
							for(var i =0 ; i < selects.length ; i++){
								var val = $(selects[i]).val();
								if(selected.indexOf(val) > -1){
									alert("请完整选择参赛球队");
									return;
								}
								if(matchInfo.indexOf(val) > -1){
									alert("请完整选择参赛球队");
									return;
								}
								selected += val + ",";
							}
							var saves = $("#round" + currentRound).find("button.save");
							for(var i = 0 ; i < saves.length ; i++){
								var s = $(saves[i]);
								var span = s.parent();
								selected2 = "";
								span.find("select").each(function(){
									selected2 += $(this).val() + "-";
								});
								selected2 = selected2.substring(0, selected2.length - 1);
								var leagueGameId = span.children("input[name='gameId']").val();
								$.ajax({
									url : contextPath + '/leagueGame/updateGame.do',
									type : "POST",
									dataType : 'json',
									async: false,
									data : {
										leagueId:leagueId,
										leagueGameId:leagueGameId,
										match:selected2,
										round:(currentRound)
									},
									beforeSend : function() {
										showLoader();
									},
									complete : function() {
										hideLoader();
									},
									success : function(rs, textStatus, jqXHR) {
										if (rs.result == "success") {
											
											//var data = rs.data;
											//showMessage("操作成功");
											//var div = span.parent();
											//div.empty();
											//var content ="";
											//content += '<input type="hidden" value="'+data.guestTeamId+'-' +data.homeTeamId +'" class="matchInfo" />' +'第' + (1) + '场 ： ' + '<span><select class="select'+(currentRound )+'">'+teamOptions+'</select>';
											//content += " VS ";
											//content += '<select class="select'+(currentRound )+'">'+teamOptions+'</select>';
											//content += '&nbsp;&nbsp;<input type="hidden" value="'+data.id+'" /><button class="btn1 btn btn-default editMatch" type="button">修改</button></span>';
											//content += '<span>&nbsp;&nbsp;<input type="hidden" value="'+data.id+'" name="gameId" />'+data.guestTeamName+' <input name="guestTeamScore" type="text" style="width:60px;" /> : <input name="homeTeamScore" type="text" style="width:60px;" /> '+data.homeTeamName + ' &nbsp;&nbsp;<button class="btn1 btn btn-default statistics" type="button">保存</button></span>';
											//div.append(content);
											//div.find("select:eq(0)").val(data.guestTeamId);
											//div.find("select:eq(1)").val(data.homeTeamId);
											//div.find("select:eq(0)").prop("disabled","disabled");
											//div.find("select:eq(1)").prop("disabled","disabled");
										} else {
											showMessage("操作失败!!!!");
										}

									},
									error : function() {
										showMessage("请稍后再试!!!");
									}
								});	
							}
							location.reload();	
						}
					});
					$(".statistics").on("click",function(){
						var span = $(this).parent();
						var leagueGameId = span.children("input[name='gameId']").val();
						var guestTeamScore = span.children("input[name='guestTeamScore']").val();
						if(guestTeamScore == 0){
							alert("请输入客队的比分");
							return;
						}
						var homeTeamScore = span.children("input[name='homeTeamScore']").val();
						if(guestTeamScore == 0){
							alert("请输入主队的比分");
							return;
						}
						$.ajax({
							url : contextPath + '/leagueGame/setScore.do',
							type : "POST",
							dataType : 'json',
							data : {
								leagueGameId:leagueGameId,
								guestTeamScore : guestTeamScore,
								homeTeamScore : homeTeamScore
							},
							beforeSend : function() {
								showLoader();
							},
							complete : function() {
								hideLoader();
							},
							success : function(rs, textStatus, jqXHR) {
								if (rs.result == "success") {
									location.reload();	
									//var data = rs.data;
									//showMessage("操作成功");
									//span.empty().append( "&nbsp;&nbsp;比分  ： " + guestTeamScore + ' : ' + homeTeamScore);
									//canContinue--;
									//span.parent().empty();
									//var content = "";
									//content += data.guestTeamName + " VS " + data.homeTeamName ;
									//if(data.guestTeamScore > data.homeTeamScore){
									//	content += '&nbsp;&nbsp;' + data.guestTeamName + "赢"  + " &nbsp;&nbsp; 比分 ： " +  data.guestTeamScore +' : '+ data.homeTeamScore;
									//}else{
									//	content += '&nbsp;&nbsp;' + data.homeTeamName + "赢"  + " &nbsp;&nbsp; 比分 ： " +  data.guestTeamScore +' : '+ data.homeTeamScore;
									//}
									//span.parent().append(content);
									//getOnGameTeam();
								} else {
									showMessage("操作失败!!!!");
								}

							},
							error : function() {
								showMessage("请稍后再试!!!");
							}
						});	
					});
				}
			}
			
			function getOnGameTeam(){
				teamIdList = new Array();
				teamList= new Array();
				teamOptions = "";
				$.ajax({
					url : contextPath + '/leagueTeam/getTeam.do',
					type : "POST",
					dataType : 'json',
					async: false,
					data : {
						leagueId:leagueId,
						pageNo : 0,
						pageSize : 1000000
					},
					beforeSend : function() {
						showLoader();
					},
					complete : function() {
						hideLoader();
					},
					success : function(rs, textStatus, jqXHR) {
						if (rs.result == "success") {
							var data = rs.data.dataList;
							if(data.length > 0){
								teamCount = data.length;
								onGameCount = data.length;
								index = 0;
								gameCount = parseInt(data.length / 2);
								teamOptions = '<option value="-1">请选择参赛球队</option>';
								for(var i = 0 ; i < data.length ; i++){
									teamIdList[i] = data[i].teamId;
									teamList[data[i].teamId] = data[i].name;
									teamOptions += '<option value="'+data[i].teamId+'">' + data[i].name + '</option>';
								}
							}else{
								showMessage("联赛暂无球队报名!!");
							}
						} else {
							showMessage("获取数据失败,请稍后再试");
						}

					},
					error : function() {
						showMessage("请稍后再试");
					}
				});
			}
			
			function getLeagueTeam(){
				$.ajax({
					url : contextPath + '/leagueTeam/getAllTeam.do',
					type : "POST",
					dataType : 'json',
					async: false,
					data : {
						leagueId:leagueId,
						pageNo : 0,
						pageSize : 1000000
					},
					beforeSend : function() {
						showLoader();
					},
					complete : function() {
						hideLoader();
					},
					success : function(rs, textStatus, jqXHR) {
						if (rs.result == "success") {
							var data = rs.data.dataList;
							if(data.length > 0){
								teamCount = data.length;
								totalRound = 1;
								var t = 0;
								while(t < teamCount){
									t = Math.pow(2,totalRound);
									totalRound++;
								}
								totalRound--;
							}else{
								$("#randomMatch").prop("disabled","disabled");
								$("#saveMatch").prop("disabled","disabled");
								showMessage("联赛暂无球队报名!!");
							}
						} else {
							showMessage("获取数据失败,请稍后再试");
						}

					},
					error : function() {
						showMessage("请稍后再试");
					}
				});		
			}
			
			function getCurrentRound(){
				for(var i = 1 ; i <= totalRound ; i++){
					var flag = false;
					$.ajax({
						url : contextPath + '/leagueGame/getGame.do',
						type : "POST",
						dataType : 'json',
						async: false,
						data : {
							leagueId:leagueId,
							round:i,
							pageNo:0,
							pageSize:10000
						},
						beforeSend : function() {
							showLoader();
						},
						complete : function() {
							hideLoader();
						},
						success : function(rs, textStatus, jqXHR) {
							if (rs.result == "success") {
								currentRound ++;
								var data = rs.data.dataList;
								if(data.length > 0){
									dataList[i] = data;
								}else{
									flag = true;
								}
							} else {
								showMessage("获取数据失败,请稍后再试");
							}

						},
						error : function() {
							showMessage("请稍后再试");
						}
					});	
					if(flag){
						break;
					}
				}
			}
			
			function getTotalRount(){
				
				
			}
			
			function addTabs(){
				var tab = $("#myTab");
				var tabContent = $("#tabContent");
				var tabs = "";
				var content = "";
				for(var i = 1 ; i <= totalRound; i++ ){
					if(i == currentRound){
						tabs += '<li role="presentation" class="active"><a href="#round'+i+'" aria-controls="home" role="tab" data-toggle="tab">第'+i+'轮</a></li>';
						content += '<div role="tabpanel" class="tab-pane active" id="round'+i+'"></div>';
					}else{
						tabs += '<li role="presentation"><a href="#round'+i+'" aria-controls="profile" role="tab" data-toggle="tab">第'+i+'轮</a></li>';
						content += '<div role="tabpanel" class="tab-pane" id="round'+i+'"></div>';
					}
				}
				tab.append(tabs);
				tabContent.append(content);
			}
			
			function bindOldData(){
				$.ajax({
					url : contextPath + '/league/get.do',
					type : "POST",
					dataType : 'json',
					data : {
						id:leagueId
					},
					beforeSend : function() {
						showLoader();
					},
					complete : function() {
						hideLoader();
					},
					success : function(rs, textStatus, jqXHR) {
						if (rs.result == "success") {
							var data = rs.data;
							$("#title").val(data.title);
							$("#startDate").val(parseDate(data.startDate));
							$("#endDate").val(parseDate(data.endDate));
							$("#area").val(data.area);
							$("#rule").val(data.rule);
							$("#advImage").attr("src",contextPath + "/" + data.adv);
						} else {
							showMessage("获取数据失败,请稍后再试");
						}

					},
					error : function() {
						showMessage("请稍后再试");
					}
				});	
			}
			
			
			
			function teamMatched(){
				
			}
			
			function match(){ 
				if(canContinue != 0){
					return;
				}
				//var matchedTeam = "";
				//$(".matchInfo").each(function(){
				//	matchedTeam += $(this).val() + ",";
				//});
				//matchedTeam = matchedTeam.substring(0,matchedTeam.length - 1);
				//var matchedArr = matchedTeam.split(/[,-]/);
				
				for(var i = 0 ; i < gameCount ; i++){
					$("#round"+currentRound).append('<div id="game'+i+'" style="height:40px;"><select id="select1'+i+'" style="width:150px;">'+teamOptions+'</select>&nbsp;VS&nbsp;<select id="select2'+i+'"  style="width:150px;">'+teamOptions+'</select></div>');
				}
				
			}
			
			$("#saveMatch").bind("click",function(){
				if(canContinue != 0){
					return;
				}
				var matchedTeam = "";
				//$(".matchInfo").each(function(){
				//	matchedTeam += $(this).val() + ",";
				//});
				//matchedTeam = matchedTeam.substring(0,matchedTeam.length - 1);
				var round = $("#round" + currentRound) ;
				for(var i = 0 ; i < gameCount ; i++){
					var game = round.find("div#game"+i);
					var selects = game.find("select");
					var team1 = $(selects[0]);
					var team2 = $(selects[1]);
					if(team1.val() == -1 ){
						showMessage("请完整选择参赛信息!!!");
						return;
					}
					if(matchedTeam.indexOf(team1.val()) > -1){
						showMessage("请完整选择参赛信息!!!");
						return;
					}
					matchedTeam += team1.val() + "-";
					
					if(team2.val() == -1 ){
						showMessage("请完整选择参赛信息!!!");
						return;
					}
					if(matchedTeam.indexOf(team2.val()) > -1){
						showMessage("请完整选择参赛信息!!!");
						return;
					}
					matchedTeam += team2.val();
					matchedTeam += ",";
				}
				
				if(i < gameCount){
					showMessage("请完整选择参赛信息!!!");
					return;
				}
				matchedTeam = matchedTeam.substring(0,matchedTeam.length - 1);
				alert(matchedTeam);
				saveMatch(matchedTeam);
			});
			
			$("#randomMatch").bind("click",function(){
				if(canContinue != 0){
					showMessage("上一轮比赛还没结束,无法安排");
					return;
				}
				var matchInfo = "";
				var temp = "";
				for(var i = 0 ; i < gameCount ;i++){
					var random1 = parseInt(teamCount*Math.random());
					var random2 = parseInt(teamCount*Math.random());
					while(random1 == random2 || temp.indexOf(random1) > -1 || temp.indexOf(random2) > -1){
						random1 = parseInt(teamCount*Math.random());
						random2 = parseInt(teamCount*Math.random());
					}
					temp += random1 + "-" + random2 + ",";
					matchInfo += teamIdList[random1] + "-" + teamIdList[random2] + ",";
				}
				matchInfo = matchInfo.substring(0,matchInfo.length - 1);
				//saveMatch(matchInfo);
				showMatch(matchInfo);
			});
			
			function saveMatch(matchedTeam){
				$.ajax({
					url : contextPath + '/leagueGame/add.do',
					type : "POST",
					dataType : 'json',
					data : {
						match:matchedTeam,
						leagueId:leagueId,
						round:currentRound
					},
					beforeSend : function() {
						showLoader();
					},
					complete : function() {
						hideLoader();
					},
					success : function(rs, textStatus, jqXHR) {
						if (rs.result == "success") {
							location.reload();	
						} else {
							showMessage("获取数据失败,请稍后再试");
						}

					},
					error : function() {
						showMessage("请稍后再试");
					}
				});	
			}
			
			function showMatch(matchInfo){
				var teamMatch = $("#round" + currentRound);
				teamMatch.empty();
				var tempArr = matchInfo.split(",");
				var content = "";
				for(var i = 0 ; i < tempArr.length ;i++){
					content += '<div id="game'+i+'" style="height:40px;">';
					content += '<input type="hidden" value="'+tempArr[i]+'" class="matchInfo'+currentRound+'" />';
					content += '第' + (i+1) + '场 ： ' ;
					content += '<select id="select1' + i + '">' + teamOptions + '</select>';
					content += ' VS ';
					content += '<select id="select2' + i + '">' + teamOptions + '</select>';
					content += '<br/>'+'</div>';
				}
				teamMatch.append(content);
				for(var i = 0 ; i < tempArr.length ;i++){
					$("#game" + i).children("select:eq(0)").val(tempArr[i].split("-")[0]);
					$("#game" + i).children("select:eq(1)").val(tempArr[i].split("-")[1]);
				}
			}
		</script>
	</body>
</html>

