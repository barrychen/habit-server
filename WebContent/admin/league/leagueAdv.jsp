<%@ page language="java" pageEncoding="UTF-8"%> 
<%@page import="com.cyou.fz.common.web.WebContext"%>
<%@page import="com.cyou.fz.pay.business.po.AdminUserPO"%>
<%@page import="com.cyou.fz.pay.business.constant.SessionKeyConstant"%>
<%
String contextPath = request.getContextPath();
AdminUserPO adminUser = (AdminUserPO) WebContext
.getSessionAttribute(SessionKeyConstant.ADMIN_LOGIN_USER);
if(adminUser == null){
	// 未登录
	response.sendRedirect(contextPath + "/login.jsp?state=" + "session-invalid");
	return;
}
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+contextPath+"/";
String leagueId = request.getParameter("id");
%>
<%@page import="java.util.UUID"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
		<link href="../css/base.css" rel="stylesheet" />
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="../assets/css/font-awesome.min.css" />

		<!--[if IE 7]>
		  <link rel="stylesheet" href="../assets/css/font-awesome-ie7.min.css" />
		<![endif]-->

		

		<link rel="stylesheet" href="../assets/css/ace.min.css" />
		<link rel="stylesheet" href="../assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="../assets/css/ace-skins.min.css" />
		<link rel="stylesheet" href="../assets/css/datepicker.css" />
		<link rel="stylesheet" href="../css/base.css" />
		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="../assets/css/ace-ie.min.css" />
		<![endif]-->

		

		<script src="../assets/js/ace-extra.min.js"></script>


		<!--[if lt IE 9]>
		<script src="../assets/js/html5shiv.js"></script>
		<script src="../assets/js/respond.min.js"></script>
		<![endif]-->
       	<script type="text/javascript">  
       		var contextPath='<%=contextPath%>';
       		var leagueId = '<%=leagueId%>';
		</script>
		<link rel="stylesheet" href="../assets/css/jquery-ui-1.10.3.custom.min.css" />
		<link rel="stylesheet" href="../assets/css/chosen.css" />
	</head>
	<body style="padding:0;margin:0;background-color:#fff;">
		<div class="breadcrumbs" id="breadcrumbs">
			<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="leagueList.jsp">赛事赛程管理</a>
							</li>
							<li class="active">赛事赛程列表</li>
						</ul>
		</div>
		<div class="container-fluid" style="padding:0 20px;font-size:12px;margin-top:5px;">
			<div class="row-fluid">
				<div class="span12">
					<div class="panel panel-default">
					  <div class="panel-heading">
							<span class="icon">
								<i class="icon-plus"></i>
							</span>
							<h5>赛事赛程</h5>
					  </div>
					  <div class="panel-body" style="padding:20px 0px 0px 0px;background-color:#F9F9F9;">
					  		<div style="margin-bottom: 10px;margin-left:20px;">
								<button type="button" class="btn btn-primary btn-sm" id="add">
									<i class="icon-plus"></i>添加赛事赛程
								</button>
							</div>
							<ul class="list-group" id="advList">
							  
							  
							</ul>
							
					  </div>
					</div>
				</div>
			</div>
		</div>
<div class="popover" style="z-index:9999;width:150px;position:absolute;left:45%;"><div class="arrow"></div><div class="popover-inner"><h3 class="popover-title">操作提示</h3><div class="popover-content" id="popover-content"><p>操作成功</p></div></div></div>
<div id="addAdv" style="display:none;z-index: 9999; width: 800px; position: absolute; left: 25%;top:10%;border:1px solid #ccc;border-radius: 4px !important;">
		<div class="arrow"></div>
		<div class="popover-inner" style="">
			<h3 class="popover-title" id="">赛事赛程管理</h3>
			<div class="popover-content" id="" style="background-color:#F5F5F5;">
				<form class="form-horizontal" style="padding-top:10px;" method="post" id="cityImageForm" enctype="multipart/form-data">
					<input type="hidden" value="<%=leagueId%>" id="leagueId" name="leagueId" />
					<input type="hidden" value="" id="id" />
					<input type="hidden" value="add" id="action" />
					<div class="form-group">
						<label for="" class="col-sm-2 control-label">描述：</label>
						<div class="col-sm-9">
							 <textarea rows="5" name="description" id="description" class="form-control"></textarea>
						</div>
						<span class="input_tip" style="line-height:30px;">*</span>
					</div>
					<div class="form-group">
						<label for="name" class="col-sm-2 control-label">图片：</label>
						<div class="col-sm-9">
							<input type="file"  name="image" id="image" />
							<img id="advImage" style="display:none" src="" width="300" height="200" />
						</div>
					</div>
					<div class="form-group">
						<label for="name" class="col-sm-2 control-label">顺序：</label>
						<div class="col-sm-9">
							<input type="text"  name="orderNum" id="orderNum" />
						</div>
					</div>
				</form>
			</div>
			<div class="popover-footer" style="background-color:#F5F5F5;overflow: hidden;">
				<button type="button" class="btn btn-primary" style="float: right;padding: 0px;margin: 0px 5px 5px 0px;" id="cancel">
						<i class="icon-ok"></i>取消
				</button>
				<button type="button" class="btn btn-danger" style="float: right;padding: 0px;margin: 0px 5px 5px 0px;" id="save">
						<i class="icon-edit"></i>保存
				</button>
				
			</div>
		</div>
	</div>
<div id="loading" style="width:32px;height:32px;position:absolute;left:40%;top:40%;display:none;">
	<img src="<%=contextPath%>/admin/assets//css/images/loading.gif" width="32" height="32" />
</div>
		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='../assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script type="text/javascript">
			window.jQuery || document.write("<script src='../assets/js/jquery-2.0.3.min.js'>"+"<"+"script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
		<script type="text/javascript">
		window.jQuery || document.write("<script src='../assets/js/jquery-1.10.2.min.js'>"+"<"+"script>");
		</script>
		<![endif]-->
		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='../assets/js/jquery.mobile.custom.min.js'>"+"<"+"script>");
		</script>
		<script src="../assets/js/bootstrap.min.js"></script>
		<script src="../assets/js/typeahead-bs2.min.js"></script>

		<!-- page specific plugin scripts -->

		<!--[if lte IE 8]>
		  <script src="../assets/js/excanvas.min.js"></script>
		<![endif]-->

		<script src="../assets/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="../assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="../assets/js/jquery.slimscroll.min.js"></script>
		<script src="../assets/js/jquery.easy-pie-chart.min.js"></script>
		<script src="../assets/js/jquery.sparkline.min.js"></script>
		<script src="../assets/js/date-time/bootstrap-datepicker.min.js"></script>
		<script src="../assets/js/fuelux/fuelux.spinner.min.js"></script>
		<script src="../assets/js/ace-elements.min.js"></script>
		<script src="../assets/js/ace.min.js"></script>
		<!-- inline scripts related to this page -->
		<script src="../assets/js/jquery.dataTables.bootstrap.js"></script>
		<script src="../assets/js/jquery-form.js"></script>
		<script src="../js/config.js"></script>
		<script src="../js/common.js"></script>
		<script type="text/javascript" src="../ckeditor/ckeditor.js"></script> 
		
		<script>
			init();
			function init(){
				$('#orderNum').ace_spinner({value:1,min:1,max:200,step:1, btn_up_class:'btn-info' , btn_down_class:'btn-info'})
				.on('change', function(){
				});
				getData();
			}
			
			function getData(){
				$.ajax({
					url : contextPath + '/league/listAdv.do',
					type : "POST",
					dataType : 'json',
					data : {
						leagueId:leagueId
					},
					beforeSend : function() {
						showLoader();
					},
					complete : function() {
						hideLoader();
					},
					success : function(rs, textStatus, jqXHR) {
						if (rs.result == "success") {
							var root =  rs.data;
							if(root.length > 0 ){
								parseData(root);
							}else{
								showMessage("无数据!!!");
							}
						} else {
							showMessage("获取数据失败,请稍后再试");
						}

					},
					error : function() {
						showMessage("请稍后再试");
					}
				});	
			}
			
			function parseData(data) {
				var dataList = $("#advList");
				dataList.empty();
				var li = "";
				var index= 0 ;
				var  i = 0;
				var length = parseInt(data.length/2);
				for (i = 0; i < length; i++) {
					li += '<li class="list-group-item" style="padding:0px;"><div class="row"><div class="col-md-6" style="border-right:1px solid #ccc">';
					li += '<div class="col-md-5" style="padding:5px;"><img src="'+contextPath + '/' +data[index].image+'" width="300px" height="200px" /></div>'; 
					li += '<div class="col-md-7" style="padding:10px 0px 0px 20px;"><div>&nbsp;&nbsp;<a href="javascript:void(0)" onclick="edit('+data[index].id+')">修改</a></div><div>'+data[index].description+'</div></div></div>';
					index++;
					li += '<div class="col-md-6" style="border-left:1px solid #ccc"><div class="col-md-5" style="padding:5px;"><img src="'+contextPath + '/'+data[index].image+'" width="300px" height="200px" /></div>';
					li += '<div class="col-md-7" style="padding:10px 0px 0px 20px;"><div>&nbsp;&nbsp;<a href="javascript:void(0)" onclick="edit('+data[index].id+')">修改</a></div><div>'+data[index].description+'</div></div></div>';
					index++;
				}
				if(index < data.length){
					li += '<li class="list-group-item" style="padding:0px;"><div class="row"><div class="col-md-6" style="border-right:1px solid #ccc">';
					li += '<div class="col-md-5" style="padding:5px;"><img src="'+contextPath + '/' +data[index].image+'" width="300px" height="200px" /></div>'; 
					li += '<div class="col-md-7" style="padding:10px 0px 0px 20px;"><div>&nbsp;&nbsp;<a href="javascript:void(0)" onclick="edit('+data[index].id+')">修改</a></div><div>'+data[index].description+'</div></div></div>';
					li += '<div class="col-md-6">';
					li += '</div></div></div></li>';
				}
				dataList.append(li);
			}
			
			$("#add").bind("click",function(){
				$("#addAdv").show();
				$("#description").val("");
				$("#image").val("");
				$("#action").val("add");
				$("#oderNum").val("0");
				$("#advImage").hide().attr("src","");
			});
			
			
			function edit(id){
				$("#id").val(id);
				$("#action").val("edit");
				$.ajax({
					url : contextPath + '/league/getAdv.do',
					type : "POST",
					dataType : 'json',
					data : {
						id:id
					},
					beforeSend : function() {
						showLoader();
					},
					complete : function() {
						hideLoader();
					},
					success : function(rs, textStatus, jqXHR) {
						if (rs.result == "success") {
							var data = rs.data;
							$("#addAdv").show();
							$("#description").text(data.description);
							$("#oderNum").val(data.orderNum);
							$("#advImage").show().attr("src",contextPath + "/" + data.image);
							CKEDITOR.instances.description.setData(data.description);
						} else {
							showMessage("请稍后再试");
						}

					},
					error : function() {
						showMessage("请稍后再试");
					}
				});
			}
			
			$("#save").bind("click",function(){
				var image = $("#image");
				var description = $("#description");
				if(description.val() == ""){
					showMessage("请输入描述!!!");
					return;
				}
				var url = "";
				if($("#action").val() == "add"){
					if(image.val() == ""){
						showMessage("请选择图片!!!");
						return;
					}
					url = '/admin/league/addAdv.do';
				}else{
					url = '/admin/league/updateAdv.do?id=' + $("#id").val();
				}
				$("#cityImageForm").ajaxSubmit({
					type: "POST",
					url: contextPath + url,
					dataType:'json',
					success: function (data) {
						if(data.result == "success"){
							showMessage(data.message);
							$("#description").val("");
							image.val("");
							$("#oderNum").val("0");
							$("#addAdv").hide();
							getData();
						}else{
							showMessage("添加失败");
						}
						$("#loading").hide();
					},
					error: function (msg) {
						$("#loading").hide();
						showMessage("添加失败");
					}
				});
			});
			
			$("#cancel").bind("click",function(){
				$("#description").val("");
				$("#image").val("");
				$("#oderNum").val("0");
				$("#addAdv").hide();
			});
		</script>
	</body>
</html>

