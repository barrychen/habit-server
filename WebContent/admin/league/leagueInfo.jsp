<%@ page language="java" pageEncoding="UTF-8"%> 
<%@page import="com.cyou.fz.common.web.WebContext"%>
<%@page import="com.cyou.fz.pay.business.po.AdminUserPO"%>
<%@page import="com.cyou.fz.pay.business.constant.SessionKeyConstant"%>
<%
String contextPath = request.getContextPath();
AdminUserPO adminUser = (AdminUserPO) WebContext
.getSessionAttribute(SessionKeyConstant.ADMIN_LOGIN_USER);
if(adminUser == null){
	// 未登录
	response.sendRedirect(contextPath + "/login.jsp?state=" + "session-invalid");
	return;
}
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+contextPath+"/";
String leagueId = request.getParameter("id");
%>
<%@page import="java.util.UUID"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
		<link href="../css/base.css" rel="stylesheet" />
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="../assets/css/font-awesome.min.css" />

		<!--[if IE 7]>
		  <link rel="stylesheet" href="../assets/css/font-awesome-ie7.min.css" />
		<![endif]-->

		

		<link rel="stylesheet" href="../assets/css/ace.min.css" />
		<link rel="stylesheet" href="../assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="../assets/css/ace-skins.min.css" />
		<link rel="stylesheet" href="../assets/css/datepicker.css" />
		<link rel="stylesheet" href="../css/base.css" />
		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="../assets/css/ace-ie.min.css" />
		<![endif]-->

		

		<script src="../assets/js/ace-extra.min.js"></script>


		<!--[if lt IE 9]>
		<script src="../assets/js/html5shiv.js"></script>
		<script src="../assets/js/respond.min.js"></script>
		<![endif]-->
       	<script type="text/javascript">  
       		var contextPath='<%=contextPath%>';
       		var leagueId = '<%=leagueId%>';
		</script>
		<link rel="stylesheet" href="../assets/css/jquery-ui-1.10.3.custom.min.css" />
		<link rel="stylesheet" href="../assets/css/chosen.css" />
	</head>
	<body style="padding:0;margin:0;background-color:#fff;">
		<div class="breadcrumbs" id="breadcrumbs">
			<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="leagueList.jsp">赛事管理</a>
							</li>
							<li class="active">赛事信息</li>
						</ul>
		</div>
		<div class="container-fluid" style="padding:0 20px;font-size:12px;margin-top:5px;">
			<div class="row-fluid">
				<div class="span12">
					<div class="panel panel-default">
					  <div class="panel-heading">
							<span class="icon">
								<i class="icon-plus"></i>
							</span>
							<h5>赛事信息</h5>
					  </div>
					  <div class="panel-body" style="padding:0px;background-color:#F9F9F9;">
							<form class="form-horizontal" style="padding:10px 0px 0px 0px;" method="post" id="leagueForm" enctype="multipart/form-data">
							  <input type="hidden" value="" id="id" name="id" />
							  <div class="form-group">
								<label for="" class="col-sm-1 control-label">名称：</label>
								<div class="col-sm-2">
								  <input id="title" class="form-control" maxlength="50" type="text" name="title" value="" readOnly="readOnly"/>
								</div>
								
								<label for="" class="col-sm-1 control-label">联赛区域：</label>
								<div class="col-sm-2">
									<input id="area" class="form-control" maxlength="50" type="text" name="title" value="" readOnly="readOnly"/>
								</div>
								
								
									<label for="" class="col-sm-1 control-label">开始时间：</label>
									<div class="col-sm-2" style="padding:0px;">
										<div class="input-group col-xs-12 col-sm-12">
											<input class="form-control col-xs-12 col-sm-12"  type="text" name="startDate" id="startDate" readOnly="readOnly" />
										</div>
									</div>
									<label for="" class="col-sm-1 control-label">结束时间：</label>
									<div class="col-sm-2" style="padding:0px;">
										<div class="input-group col-xs-12 col-sm-12">
											<input class="form-control col-xs-12 col-sm-12"  type="text" name="endDate" id="endDate" readOnly="readOnly" />
										</div>
									</div>
							  </div>
							  <div class="form-group">
								<label for="" class="col-sm-1 control-label">规则：</label>
								<div class="col-sm-7">
								 <div id="rule"></div>
								</div>
							  </div>
							  
							  <div class="form-group">
								<label for="name" class="col-sm-1 control-label">广告图片：</label>
								<div class="col-sm-8">
									<img id="advImage" src="" width="200" height="100" />
								</div>
							  </div>
							</form>
					  </div>
					</div>
				</div>
				
				<div class="col-xs-12">
				<div class="table-responsive">
					<input type="hidden" value="1" id="start" />
					<table id="sample-table-2"
						class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th class="center"><label> <input type="checkbox"
										class="ace" /> <span class="lbl"></span>
								</label></th>
								<th>队伍</th>
								<th>队长</th>
								<th>球员数量</th>
								<th>比赛数</th>
								<th>联赛积分数</th>
								<th>总积分数</th>
								<th>赢球数</th>
								<th>平球数</th>
								<th>输球熟</th>
								<th>报名时间</th>
								<th>状态</th>
								<th>操作</th>
							</tr>
						</thead>

						<tbody id="data-list">

						</tbody>
					</table>
				</div>
				
				<div class="row">
			<div class="col-sm-6">
				<div id="sample-table-2_info" class="dataTables_info">
					当前 <span id="current"></span> 到 <span id="page"></span> 总共 <span
						id="total"></span> 条
				</div>
			</div>
			<div class="col-sm-6">
				<div class="dataTables_paginate paging_bootstrap">
					<ul class="pagination pagination-sm" id="page-list">

					</ul>
				</div>
			</div>
		</div>
			</div>
				
				
			</div>
		</div>
<div class="popover" style="z-index:9999;width:150px;position:absolute;left:45%;"><div class="arrow"></div><div class="popover-inner"><h3 class="popover-title">操作提示</h3><div class="popover-content" id="popover-content"><p>操作成功</p></div></div></div>

<div id="loading" style="width:32px;height:32px;position:absolute;left:40%;top:40%;display:none;">
	<img src="<%=contextPath%>/admin/assets//css/images/loading.gif" width="32" height="32" />
</div>
		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='../assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script type="text/javascript">
			window.jQuery || document.write("<script src='../assets/js/jquery-2.0.3.min.js'>"+"<"+"script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
		<script type="text/javascript">
		window.jQuery || document.write("<script src='../assets/js/jquery-1.10.2.min.js'>"+"<"+"script>");
		</script>
		<![endif]-->
		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='../assets/js/jquery.mobile.custom.min.js'>"+"<"+"script>");
		</script>
		<script src="../assets/js/bootstrap.min.js"></script>
		<script src="../assets/js/typeahead-bs2.min.js"></script>

		<!-- page specific plugin scripts -->

		<!--[if lte IE 8]>
		  <script src="../assets/js/excanvas.min.js"></script>
		<![endif]-->

		<script src="../assets/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="../assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="../assets/js/jquery.slimscroll.min.js"></script>
		<script src="../assets/js/jquery.easy-pie-chart.min.js"></script>
		<script src="../assets/js/jquery.sparkline.min.js"></script>
		<script src="../assets/js/date-time/bootstrap-datepicker.min.js"></script>
		<script src="../assets/js/ace-elements.min.js"></script>
		<script src="../assets/js/ace.min.js"></script>
		<!-- inline scripts related to this page -->
		<script src="../assets/js/jquery.dataTables.bootstrap.js"></script>
		<script src="../assets/js/jquery-form.js"></script>
		<script src="../js/config.js"></script>
		<script src="../js/common.js"></script>
		<script src="../js/city.js"></script>
		
		<script>
			init();
			function init(){
				bindOldData();
				getLeagueTeam(1);
			}
			function bindOldData(){
				$.ajax({
					url : contextPath + '/league/get.do',
					type : "POST",
					dataType : 'json',
					data : {
						id:leagueId
					},
					beforeSend : function() {
						showLoader();
					},
					complete : function() {
						hideLoader();
					},
					success : function(rs, textStatus, jqXHR) {
						if (rs.result == "success") {
							var data = rs.data;
							$("#title").val(data.title);
							$("#startDate").val(parseDate(data.startDate));
							$("#endDate").val(parseDate(data.endDate));
							$("#area").val(data.area);
							$("#rule").html(data.rule);
							$("#advImage").attr("src",contextPath + "/" + data.adv);
						} else {
							showMessage("获取数据失败,请稍后再试");
						}

					},
					error : function() {
						showMessage("请稍后再试");
					}
				});	
			}
			function getLeagueTeam(start){
				$("#start").val(start);
				$.ajax({
					url : contextPath + '/leagueTeam/getAllTeam.do',
					type : "POST",
					dataType : 'json',
					data : {
						leagueId:leagueId,
						pageNo : (start - 1) * pageSize,
						pageSize : pageSize
					},
					beforeSend : function() {
						showLoader();
					},
					complete : function() {
						hideLoader();
					},
					success : function(rs, textStatus, jqXHR) {
						if (rs.result == "success") {
							var data = rs.data;
							var total = data.totalCount;
							var root = data.dataList;
							var dataList = $("#data-list");
							dataList.empty();
							$("#total").text(total);
							$("#current").text((start - 1) * pageSize + 1);
							if (root.length > 0) {
								$("#page").text(
										(start - 1) * pageSize + root.length);
								parseData(root, total, start);
							} else {
								showMessage("无参赛球队信息");
							}
						} else {
							showMessage("获取数据失败,请稍后再试");
						}

					},
					error : function() {
						showMessage("请稍后再试");
					}
				});		
			}
			var teamState = ['<span class="label label-success">参赛</span>' , '<span class="label label-default">淘汰</span>'];
			function parseData(data, total, start) {
				var dataList = $("#data-list");
				var tr = "";
				for (var i = 0; i < data.length; i++) {
					tr += '<tr><td class="center"><label><input type="checkbox" value="'+data[i].id+'" class="ace" /><span class="lbl"></span></label></td>';
					tr += '<td>' + data[i].name + '</td>';
					tr += '<td>' + data[i].captainName + '</td>';
					tr += '<td>' + data[i].playerCounts + '</td>';
					tr += '<td>' + data[i].gameCounts + '</td>';
					tr += '<td>' + data[i].leaguePoint + '</td>';
					tr += '<td>' + data[i].pointCounts + '</td>';
					tr += '<td>' + data[i].winCounts + '</td>';
					tr += '<td>' + data[i].drawCounts + '</td>';
					tr += '<td>' + data[i].lostCounts + '</td>';
					tr += '<td>' + parseDate(data[i].createTime) + '</td>';
					tr += '<td>' + teamState[data[i].status] + '</td>';
					tr += '<td><div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">';
					tr += "";
					tr += '</div></td>';
					tr += '</tr>';
				}
				dataList.append(tr);
				//分页
				var pageList = $("#page-list");
				pageList.empty();
				pageList.append(getPageList(start, total));
			}
		</script>
	</body>
</html>

