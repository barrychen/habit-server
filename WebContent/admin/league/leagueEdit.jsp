<%@ page language="java" pageEncoding="UTF-8"%> 
<%@page import="com.cyou.fz.common.web.WebContext"%>
<%@page import="com.cyou.fz.pay.business.po.AdminUserPO"%>
<%@page import="com.cyou.fz.pay.business.constant.SessionKeyConstant"%>
<%
String contextPath = request.getContextPath();
AdminUserPO adminUser = (AdminUserPO) WebContext
.getSessionAttribute(SessionKeyConstant.ADMIN_LOGIN_USER);
if(adminUser == null){
	// 未登录
	response.sendRedirect(contextPath + "/login.jsp?state=" + "session-invalid");
	return;
}
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+contextPath+"/";
String leagueId = request.getParameter("id");
String action = "add";
if(leagueId != null){
	action = "edit";
}
%>
<%@page import="java.util.UUID"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
		<link href="../css/base.css" rel="stylesheet" />
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="../assets/css/font-awesome.min.css" />

		<!--[if IE 7]>
		  <link rel="stylesheet" href="../assets/css/font-awesome-ie7.min.css" />
		<![endif]-->

		

		<link rel="stylesheet" href="../assets/css/ace.min.css" />
		<link rel="stylesheet" href="../assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="../assets/css/ace-skins.min.css" />
		<link rel="stylesheet" href="../assets/css/datepicker.css" />
		<link rel="stylesheet" href="../css/base.css" />
		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="../assets/css/ace-ie.min.css" />
		<![endif]-->

		

		<script src="../assets/js/ace-extra.min.js"></script>


		<!--[if lt IE 9]>
		<script src="../assets/js/html5shiv.js"></script>
		<script src="../assets/js/respond.min.js"></script>
		<![endif]-->
       	<script type="text/javascript">  
       		var contextPath='<%=contextPath%>';
       		var leagueId = '<%=leagueId%>';
       		var action = '<%=action%>';
		</script>
		<link rel="stylesheet" href="../assets/css/jquery-ui-1.10.3.custom.min.css" />
		<link rel="stylesheet" href="../assets/css/chosen.css" />
	</head>
	<body style="padding:0;margin:0;background-color:#fff;">
		<div class="breadcrumbs" id="breadcrumbs">
			<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="leagueList.jsp">赛事管理</a>
							</li>
							<li class="active">赛事编辑</li>
						</ul>
		</div>
		
		<div class="container-fluid" style="padding:0 20px;font-size:12px;margin-top:20px;">
			<div class="row-fluid">
				<div class="span12">
					<div class="panel panel-default">
					  <div class="panel-heading">
							<span class="icon">
								<i class="icon-plus"></i>
							</span>
							<h5>赛事编辑</h5>
					  </div>
					  <div class="panel-body" style="padding:0px;background-color:#F9F9F9;">
							<form class="form-horizontal" style="padding-top:10px;" method="post" id="leagueForm" enctype="multipart/form-data">
							  <input type="hidden" value="" id="id" name="id" />
							  <div class="form-group">
								<label for="" class="col-sm-2 control-label">名称：</label>
								<div class="col-sm-8">
								  <input id="title" class="form-control" maxlength="50" type="text" name="title" value=""/>
								</div>
								<span class="input_tip" style="line-height:30px;">*</span>
							  </div>
							  
							  <div class="form-group">
									<label for="" class="col-sm-2 control-label">开始时间：</label>
									<div class="col-sm-9" style="padding:0px;">
										<div class="input-group col-xs-9 col-sm-3">
											<input class="form-control col-xs-4 col-sm-3"  type="text" name="startDate" id="startDate" readOnly="readOnly" />
											<span class="input-group-addon" style="border-radius: 4px !important;">
													<i class="icon-calendar bigger-110"></i>
											</span>
										
										</div>
									<span class="input_tip" style="line-height:30px;">*</span>
									</div>
								</div>
								<div class="form-group">
									<label for="" class="col-sm-2 control-label">结束时间：</label>
									<div class="col-sm-9" style="padding:0px;">
										<div class="input-group col-xs-9 col-sm-3">
											<input class="form-control col-xs-4 col-sm-3"  type="text" name="endDate" id="endDate" readOnly="readOnly" />
											<span class="input-group-addon" style="border-radius: 4px !important;">
													<i class="icon-calendar bigger-110"></i>
											</span>
										
										</div>
									<span class="input_tip" style="line-height:30px;">*</span>
									</div>
								</div>
							  
							  <div class="form-group">
								<label for="" class="col-sm-2 control-label">规则：</label>
								<div class="col-sm-8">
								  <textarea rows="15" name="rule" id="rule" class="form-control"></textarea>
								</div>
								<span class="input_tip" style="line-height:30px;">*</span>
							  </div>
							  
							  <div class="form-group">
								<label for="" class="col-sm-2 control-label">参赛数目：</label>
								<div class="col-sm-8">
									<input type="text" name="teamCount" id="teamCount" class="input-mini col-xs-10 col-sm-5 spinner1" />
								</div>
							  </div>
							  
							  <div class="form-group">
								<label for="" class="col-sm-2 control-label">是否显示：</label>
								<div class="col-sm-8">
									<input type="radio" name="isShow" class="isShow" value="0" checked="checked" />显示
									<input type="radio" name="isShow" class="isShow" value="1" />隐藏
								</div>
							  </div>
							  
							  <div class="form-group">
								<label for="" class="col-sm-2 control-label">联赛区域：</label>
								<div class="col-sm-8">
									<input type="hidden" value="" name="area" id="area" />
									<select id="province" name="areaProvince">
										<option value="-1" selected="selected">
											请选择城市
										</option>
									</select>
									<select id="city" name="areaCity">
										<option value="-1">
											请选择城市
										</option>
									</select>
								</div>
							  </div>
							  
							  <div class="form-group">
								<label for="name" class="col-sm-2 control-label">广告图片：</label>
								<div class="col-sm-8">
									<input type="file"  name="adv" id="adv" />
									<img id="advImage" style="display:none" src="" width="200" height="100" />
								</div>
							  </div>
							  
							  <div class="space-4"></div>
							  <div class="form-group" style="padding-top:10px;">
								<div class="col-sm-offset-2 col-sm-10">
									<a class="btn btn-sm btn-primary" id="save">
										<i class="icon-ok"></i>保存
									</a>
								</div>
							  </div>
							</form>
					  </div>
					</div>
				</div>
			</div>
		</div>
<div class="popover" style="z-index:9999;width:150px;position:absolute;left:45%;"><div class="arrow"></div><div class="popover-inner"><h3 class="popover-title">操作提示</h3><div class="popover-content" id="popover-content"><p>操作成功</p></div></div></div>

<div id="loading" style="width:32px;height:32px;position:absolute;left:40%;top:40%;display:none;">
	<img src="<%=contextPath%>/admin/assets//css/images/loading.gif" width="32" height="32" />
</div>
		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='../assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script type="text/javascript">
			window.jQuery || document.write("<script src='../assets/js/jquery-2.0.3.min.js'>"+"<"+"script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
		<script type="text/javascript">
		window.jQuery || document.write("<script src='../assets/js/jquery-1.10.2.min.js'>"+"<"+"script>");
		</script>
		<![endif]-->
		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='../assets/js/jquery.mobile.custom.min.js'>"+"<"+"script>");
		</script>
		<script src="../assets/js/bootstrap.min.js"></script>
		<script src="../assets/js/typeahead-bs2.min.js"></script>

		<!-- page specific plugin scripts -->

		<!--[if lte IE 8]>
		  <script src="../assets/js/excanvas.min.js"></script>
		<![endif]-->

		<script src="../assets/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="../assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="../assets/js/jquery.slimscroll.min.js"></script>
		<script src="../assets/js/jquery.easy-pie-chart.min.js"></script>
		<script src="../assets/js/jquery.sparkline.min.js"></script>
		<script src="../assets/js/fuelux/fuelux.spinner.min.js"></script>
		<script src="../assets/js/date-time/bootstrap-datepicker.min.js"></script>
		<script src="../assets/js/ace-elements.min.js"></script>
		<script src="../assets/js/ace.min.js"></script>
		<!-- inline scripts related to this page -->
		<script src="../assets/js/jquery.dataTables.bootstrap.js"></script>
		<script src="../assets/js/jquery-form.js"></script>
		<script src="../js/config.js"></script>
		<script src="../js/common.js"></script>
		<script src="../js/city.js"></script>
		<script type="text/javascript" src="../ckeditor/ckeditor.js"></script> 
		
		<script>
			init();
			function init(){
				CKEDITOR.replace( 'rule',
						  {
							  toolbar :
							  [
								 //加粗     斜体，下划线     穿过线    下标字      上标字
								 ['Bold','Italic','Underline','Strike','Subscript','Superscript'],
								  //数字列表       实体列表          减小缩进  增大缩进
								 ['NumberedList','BulletedList','-','Outdent','Indent'],
								  //左对齐        居中对齐           右对齐      两端对齐
								 ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
								//超链接  取消超链接 锚点
								 ['Link','Unlink','Anchor'],
								 //图片    flash   表格     水平线          表情      特殊字符      分页符
								 ['Table','HorizontalRule','Smiley','SpecialChar','PageBreak'],
								'/',
								  //样式     格式     字体   字体大小
								 ['Styles','Format','Font','FontSize'],
								 //文本颜色    背景颜色
								 ['TextColor','BGColor'],
								  //全屏       显示区块
								 ['Maximize', 'ShowBlocks','-']
							  ]
						  }
					 );
				bindProvince($("#province"),$("#city"));
				if(action == "edit"){
					bindOldData();
				}
				$("#startDate").datepicker({format: 'yyyy-mm-dd',autoclose:true}).next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
				$("#endDate").datepicker({format: 'yyyy-mm-dd',autoclose:true}).next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
				
				$('#teamCount').ace_spinner({value:1,min:1,max:200,step:5, btn_up_class:'btn-info' , btn_down_class:'btn-info'})
					.on('change', function(){
				});
				$("#save").bind("click",function(){
					if(action == "add"){
						save();
					}else{
						edit();
					}
					
				});
			}
			
			function bindOldData(){
				$.ajax({
					url : contextPath + '/admin/league/get.do',
					type : "POST",
					dataType : 'json',
					data : {
						id:leagueId
					},
					beforeSend : function() {
						showLoader();
					},
					complete : function() {
						hideLoader();
					},
					success : function(rs, textStatus, jqXHR) {
						if (rs.result == "success") {
							var data = rs.data;
							$("#id").val(data.id);
							$("#title").val(data.title);
							$("#startDate").val(parseDate(data.startDate));
							$("#endDate").val(parseDate(data.endDate));
							$("#rule").val(data.rule);
							CKEDITOR.instances.rule.setData(data.rule);
							$("#teamCount").val(data.teamCount);
							$("#province").val(data.areaProvince);
							bindCity($("#city"),data.areaProvince,data.areaCity);
							$(".isShow")[data.isShow].checked = "checked";
							$("#advImage").show().attr("src",contextPath + "/" + data.adv);
						} else {
							showMessage("获取数据失败,请稍后再试");
						}

					},
					error : function() {
						showMessage("请稍后再试");
					}
				});		
			}
			
			
			function save(){
				if(validate() == 1){
					return;
				}
				$("#loading").show();
				var rule= CKEDITOR.instances.rule.getData();
				$("#rule").val(rule);
				$("#leagueForm").ajaxSubmit({
					type: "POST",
					url: contextPath + '/admin/league/add.do',
					dataType:'json',
					success: function (data) {
						if(data.result == "success"){
							window.location.href="leagueList.jsp";
						}else{
							showMessage("添加失败");
						}
						$("#loading").hide();
					},
					error: function (msg) {
						$("#loading").hide();
						showMessage("添加失败");
					}
				});
			}
			
			
			function edit(){
				if(validate() == 1){
					return;
				}
				var rule= CKEDITOR.instances.rule.getData();
				$("#rule").val(rule);
				$("#loading").show();
				$("#leagueForm").ajaxSubmit({
					type: "POST",
					url: contextPath + '/admin/league/update.do',
					dataType:'json',
					success: function (data) {
						if(data.result == "success"){
							showMessage("更新成功");
							bindOldData();
						}else{
							showMessage("更新失败");
						}
						$("#loading").hide();
					},
					error: function (msg) {
						$("#loading").hide();
						showMessage("更新失败");
					}
				});
			}
			
			function validate(){
				var title = $("#title");
				if(title.val() == ""){
					showMessage("请输入联赛名!!");
					return 1;
				}
				var startDate = $("#startDate");
				if(startDate.val() == ""){
					showMessage("请输入开始时间!!");
					return 1;
				}
				var endDate = $("#endDate");
				if(endDate.val() == ""){
					showMessage("请输入结束时间!!");
					return 1;
				}
				var rule = $("#rule");
				if(rule.val() == ""){
					showMessage("请输入开始时间!!");
					return 1;
				}
				var rule= CKEDITOR.instances.rule.getData();
				if(rule == ""){
					showMessage("请输入比赛规则!!");
					return 1;
				}
				var province = $("#province");
				if(province.val() == -1){
					showMessage("请选择省份!!");
					return 1;
				}
				var city = $("#city");
				if(city.val() < 0 ){
					showMessage("请选择市!!");
					return 1;
				}
				if(action == "add"){
					var adv = $("#adv");
					if(adv.val() == ""){
						showMessage("请选择广告图片!!");
						return 1;
					}
				}
				
			}
			
			
			
		</script>
	</body>
</html>

