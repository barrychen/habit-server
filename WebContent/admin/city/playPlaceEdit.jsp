<%@ page language="java" pageEncoding="UTF-8"%> 
<%@page import="com.cyou.fz.common.web.WebContext"%>
<%@page import="com.cyou.fz.pay.business.po.AdminUserPO"%>
<%@page import="com.cyou.fz.pay.business.constant.SessionKeyConstant"%>
<%
String contextPath = request.getContextPath();
AdminUserPO adminUser = (AdminUserPO) WebContext
.getSessionAttribute(SessionKeyConstant.ADMIN_LOGIN_USER);
if(adminUser == null){
	// 未登录
	response.sendRedirect(contextPath + "/login.jsp?state=" + "session-invalid");
	return;
}
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+contextPath+"/";
String placeId = request.getParameter("id");
String cityId = request.getParameter("cityId");
String action = "add";
if(placeId != null){
	action = "edit";
}
%>
<%@page import="java.util.UUID"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
		<link href="../css/base.css" rel="stylesheet" />
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="../assets/css/font-awesome.min.css" />

		<!--[if IE 7]>
		  <link rel="stylesheet" href="../assets/css/font-awesome-ie7.min.css" />
		<![endif]-->

		

		<link rel="stylesheet" href="../assets/css/ace.min.css" />
		<link rel="stylesheet" href="../assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="../assets/css/ace-skins.min.css" />
		<link rel="stylesheet" href="../assets/css/datepicker.css" />
		<link rel="stylesheet" href="../css/base.css" />
		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="../assets/css/ace-ie.min.css" />
		<![endif]-->

		

		<script src="../assets/js/ace-extra.min.js"></script>


		<!--[if lt IE 9]>
		<script src="../assets/js/html5shiv.js"></script>
		<script src="../assets/js/respond.min.js"></script>
		<![endif]-->
       	<script type="text/javascript">  
       		var contextPath='<%=contextPath%>';
       		var cityId = '<%=cityId%>';
       		var placeId = '<%=placeId%>';
       		var action = '<%=action%>';
		</script>
		<link rel="stylesheet" href="../assets/css/jquery-ui-1.10.3.custom.min.css" />
		<link rel="stylesheet" href="../assets/css/chosen.css" />
	</head>
	<body style="padding:0;margin:0;background-color:#fff;">
		<div class="breadcrumbs" id="breadcrumbs">
			<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="cityPlace.jsp">城市场地管理</a>
							</li>
							<li class="active">城市场地编辑</li>
						</ul>
		</div>
		
		<div class="container-fluid" style="padding:0 20px;font-size:12px;margin-top:20px;">
			<div class="row-fluid">
				<div class="span12">
					<div class="panel panel-default">
					  <div class="panel-heading">
							<span class="icon">
								<i class="icon-plus"></i>
							</span>
							<h5>城市场地</h5>
					  </div>
					  <div class="panel-body" style="padding:0px;background-color:#F9F9F9;">
							<form class="form-horizontal" style="padding-top:10px;" method="post" id="playPlaceForm" enctype="multipart/form-data">
							  <input type="hidden" value="" id="id" name="id" />
							  <input type="hidden" value="<%=cityId%>" name="cityId" />
							  <div class="form-group">
								<label for="" class="col-sm-2 control-label">名称：</label>
								<div class="col-sm-8">
								  <input id="name" class="form-control" maxlength="50" type="text" name="name" value=""/>
								</div>
								<span class="input_tip" style="line-height:30px;">*</span>
							  </div>
							  
							  <div class="form-group">
								<label for="" class="col-sm-2 control-label">场地地址：</label>
								<div class="col-sm-8">
								 <input id="addr" class="form-control" maxlength="50" type="text" name="addr" value=""/>
								</div>
								<span class="input_tip" style="line-height:30px;">*</span>
							  </div>
							  
							  <div class="form-group">
								<label for="" class="col-sm-2 control-label">经度：</label>
								<div class="col-sm-8">
								 <input id="lon" class="form-control" maxlength="50" type="text" name="lon" value=""/>
								</div>
								<span class="input_tip" style="line-height:30px;">*</span>
							  </div>
							  
							  <div class="form-group">
								<label for="" class="col-sm-2 control-label">纬度：</label>
								<div class="col-sm-8">
								 <input id="lat" class="form-control" maxlength="50" type="text" name="lat" value=""/>
								</div>
								<span class="input_tip" style="line-height:30px;">*</span>
							  </div>
							  
							  <div class="form-group">
								<label for="" class="col-sm-2 control-label">联系电话：</label>
								<div class="col-sm-8">
								 <input id="phone" class="form-control" maxlength="50" type="text" name="phone" value=""/>
								</div>
								<span class="input_tip" style="line-height:30px;">*</span>
							  </div>
							  
							  
							  <div class="form-group">
								<label for="" class="col-sm-2 control-label">是否显示：</label>
								<div class="col-sm-8">
									<input type="radio" name="isShow" class="isShow" value="0" checked="checked" />显示
									<input type="radio" name="isShow" class="isShow" value="1" />隐藏
								</div>
							  </div>
							  
							  
							  <div class="form-group">
								<label for="name" class="col-sm-2 control-label">图片：</label>
								<div class="col-sm-8">
									<input type="file"  name="image" id="image" />
									<img id="advImage" style="display:none" src="" width="200" height="100" />
								</div>
							  </div>
							  
							  <div class="space-4"></div>
							  <div class="form-group" style="padding-top:10px;">
								<div class="col-sm-offset-2 col-sm-10">
									<a class="btn btn-sm btn-primary" id="save">
										<i class="icon-ok"></i>保存
									</a>
								</div>
							  </div>
							</form>
					  </div>
					</div>
				</div>
			</div>
		</div>
<div class="popover" style="z-index:9999;width:150px;position:absolute;left:45%;"><div class="arrow"></div><div class="popover-inner"><h3 class="popover-title">操作提示</h3><div class="popover-content" id="popover-content"><p>操作成功</p></div></div></div>

<div id="loading" style="width:32px;height:32px;position:absolute;left:40%;top:40%;display:none;">
	<img src="<%=contextPath%>/admin/assets//css/images/loading.gif" width="32" height="32" />
</div>
		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='../assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script type="text/javascript">
			window.jQuery || document.write("<script src='../assets/js/jquery-2.0.3.min.js'>"+"<"+"script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
		<script type="text/javascript">
		window.jQuery || document.write("<script src='../assets/js/jquery-1.10.2.min.js'>"+"<"+"script>");
		</script>
		<![endif]-->
		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='../assets/js/jquery.mobile.custom.min.js'>"+"<"+"script>");
		</script>
		<script src="../assets/js/bootstrap.min.js"></script>
		<script src="../assets/js/typeahead-bs2.min.js"></script>

		<!-- page specific plugin scripts -->

		<!--[if lte IE 8]>
		  <script src="../assets/js/excanvas.min.js"></script>
		<![endif]-->

		<script src="../assets/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="../assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="../assets/js/jquery.slimscroll.min.js"></script>
		<script src="../assets/js/jquery.easy-pie-chart.min.js"></script>
		<script src="../assets/js/jquery.sparkline.min.js"></script>
		<script src="../assets/js/fuelux/fuelux.spinner.min.js"></script>
		<script src="../assets/js/date-time/bootstrap-datepicker.min.js"></script>
		<script src="../assets/js/ace-elements.min.js"></script>
		<script src="../assets/js/ace.min.js"></script>
		<!-- inline scripts related to this page -->
		<script src="../assets/js/jquery.dataTables.bootstrap.js"></script>
		<script src="../assets/js/jquery-form.js"></script>
		<script src="../js/config.js"></script>
		<script src="../js/common.js"></script>
		<script src="../js/city.js"></script>
		<script type="text/javascript" src="../ckeditor/ckeditor.js"></script> 
		
		<script>
			init();
			function init(){
				if(action == "edit"){
					bindOldData();
				}
				$("#save").bind("click",function(){
					if(action == "add"){
						save();
					}else{
						edit();
					}
					
				});
			}
			
			function bindOldData(){
				$.ajax({
					url : contextPath + '/city/getPlace.do',
					type : "POST",
					dataType : 'json',
					data : {
						id:placeId
					},
					beforeSend : function() {
						showLoader();
					},
					complete : function() {
						hideLoader();
					},
					success : function(rs, textStatus, jqXHR) {
						if (rs.result == "success") {
							var data = rs.data;
							$("#id").val(data.id);
							$("#name").val(data.name);
							$("#addr").val(data.addr);
							$("#lon").val(data.lon);
							$("#lat").val(data.lat);
							$("#phone").val(data.phone);
							$(".isShow")[data.isShow].checked = "checked";
							$("#advImage").show().attr("src",contextPath + "/" + data.image);
						} else {
							showMessage("获取数据失败,请稍后再试");
						}

					},
					error : function() {
						showMessage("请稍后再试");
					}
				});		
			}
			
			
			function save(){
				if(validate() == 1){
					return;
				}
				$("#loading").show();
				$("#playPlaceForm").ajaxSubmit({
					type: "POST",
					url: contextPath + '/admin/city/addPlace.do',
					dataType:'json',
					success: function (data) {
						if(data.result == "success"){
							window.location.href="cityPlace.jsp?id=" + cityId;
						}else{
							showMessage("添加失败");
						}
						$("#loading").hide();
					},
					error: function (msg) {
						$("#loading").hide();
						showMessage("添加失败");
					}
				});
			}
			
			
			function edit(){
				if(validate() == 1){
					return;
				}
				$("#loading").show();
				$("#playPlaceForm").ajaxSubmit({
					type: "POST",
					url: contextPath + '/admin/city/updatePlace.do',
					dataType:'json',
					success: function (data) {
						if(data.result == "success"){
							showMessage("更新成功");
							bindOldData();
						}else{
							showMessage("更新失败");
						}
						$("#loading").hide();
					},
					error: function (msg) {
						$("#loading").hide();
						showMessage("更新失败");
					}
				});
			}
			
			function validate(){
				var name = $("#name");
				if(name.val() == ""){
					showMessage("请输入场地名!!");
					return 1;
				}
				var addr = $("#addr");
				if(addr.val() == ""){
					showMessage("请输入场地地址!!");
					return 1;
				}
				
				var lon = $("#lon");
				if(lon.val() == ""){
					showMessage("请输入场地经度!!");
					return 1;
				}
				
				var lat = $("#lat");
				if(lat.val() == ""){
					showMessage("请输入场地纬度!!");
					return 1;
				}
				
				var phone = $("#phone");
				
				if(phone.val() == ""){
					showMessage("请输入场地联系电话!!");
					return 1;
				}
				if(action == "add"){
					var image = $("#image");
					if(image.val() == ""){
						showMessage("请选择图片!!");
						return 1;
					}
				}
				
			}
			
		</script>
	</body>
</html>

