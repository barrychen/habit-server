<%@ page language="java" pageEncoding="UTF-8"%> 
<%@page import="com.cyou.fz.common.web.WebContext"%>
<%@page import="com.cyou.fz.pay.business.po.AdminUserPO"%>
<%@page import="com.cyou.fz.pay.business.constant.SessionKeyConstant"%>
<%
String contextPath = request.getContextPath();
AdminUserPO adminUser = (AdminUserPO) WebContext
.getSessionAttribute(SessionKeyConstant.ADMIN_LOGIN_USER);
if(adminUser == null){
	// 未登录
	response.sendRedirect(contextPath + "/login.jsp?state=" + "session-invalid");
	return;
}
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+contextPath+"/";
String id = request.getParameter("id");
String action = "add";
if(id != null){
	action = "edit";
}
%>
<%@page import="java.util.UUID"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
		<link href="../css/base.css" rel="stylesheet" />
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="../assets/css/font-awesome.min.css" />

		<!--[if IE 7]>
		  <link rel="stylesheet" href="../assets/css/font-awesome-ie7.min.css" />
		<![endif]-->

		

		<link rel="stylesheet" href="../assets/css/ace.min.css" />
		<link rel="stylesheet" href="../assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="../assets/css/ace-skins.min.css" />
		<link rel="stylesheet" href="../assets/css/datepicker.css" />
		<link rel="stylesheet" href="../css/base.css" />
		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="../assets/css/ace-ie.min.css" />
		<![endif]-->

		

		<script src="../assets/js/ace-extra.min.js"></script>


		<!--[if lt IE 9]>
		<script src="../assets/js/html5shiv.js"></script>
		<script src="../assets/js/respond.min.js"></script>
		<![endif]-->
       	<script type="text/javascript">  
       		var contextPath='<%=contextPath%>';
       		var id = '<%=id%>';
       		var action = '<%=action%>';
		</script>
		<link rel="stylesheet" href="../assets/css/jquery-ui-1.10.3.custom.min.css" />
		<link rel="stylesheet" href="../assets/css/chosen.css" />
	</head>
	<body style="padding:0;margin:0;background-color:#fff;">
		<div class="breadcrumbs" id="breadcrumbs">
			<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="cityPlace.jsp">App版本管理</a>
							</li>
							<li class="active">App版本编辑</li>
						</ul>
		</div>
		
		<div class="container-fluid" style="padding:0 20px;font-size:12px;margin-top:20px;">
			<div class="row-fluid">
				<div class="span12">
					<div class="panel panel-default">
					  <div class="panel-heading">
							<span class="icon">
								<i class="icon-plus"></i>
							</span>
							<h5>App版本</h5>
					  </div>
					  <div class="panel-body" style="padding:0px;background-color:#F9F9F9;">
							<form class="form-horizontal" style="padding-top:10px;" method="post" id="versionForm"  enctype="multipart/form-data">
							  <input type="hidden" value="<%=id%>" id="id" name="id" />
							  <div class="form-group">
								<label for="" class="col-sm-2 control-label">版本编号：</label>
								<div class="col-sm-8">
								  <input id="version" class="form-control" maxlength="50" type="text" name="version" value=""/>
								</div>
								<span class="input_tip" style="line-height:30px;">*</span>
							  </div>
							  
							  <div class="form-group">
								<label for="" class="col-sm-2 control-label">更新内容：</label>
								<div class="col-sm-8">
								<textarea rows="5" name="content" id="content" class="form-control" placeholder="如需换行，请用-"></textarea>
								</div>
								<span class="input_tip" style="line-height:30px;">*</span>
							  </div>
							  
							  <div class="form-group">
								<label for="" class="col-sm-2 control-label">下载地址：</label>
								<div class="col-sm-8">
								 <input id="url" class="form-control" maxlength="50" type="text" name="url" value=""/>
								</div>
								<span class="input_tip" style="line-height:30px;color:red;">*当上传附件不为空时，下载地址自动生成</span>
							  </div>
							  
							   <div class="form-group">
								<label for="name" class="col-sm-2 control-label">附件：</label>
								<div class="col-sm-8">
									<input type="file"  name="file" id="file" />
									<a href="" id="fileDown" style="display:none;">附件下载</a>
								</div>
								<span class="input_tip" style="line-height:30px;color:red;">*当附件为空时,下载地址必需填写</span>
							  </div>
							  
							  <div class="form-group">
								<label for="" class="col-sm-2 control-label">平台信息：</label>
								<div class="col-sm-8">
									<input type="radio" name="platform" class="platform" value="0" checked="checked" />Android
									<input type="radio" name="platform" class="platform" value="1" />ISO
								</div>
							  </div>
							  
							  
							  <div class="form-group">
								<label for="" class="col-sm-2 control-label">是否发布：</label>
								<div class="col-sm-8">
									<input type="radio" name="isRelease" class="isRelease" value="0" checked="checked" />发布
									<input type="radio" name="isRelease" class="isRelease" value="1" />暂不发布
								</div>
							  </div>
							  
							  <div class="space-4"></div>
							  <div class="form-group" style="padding-top:10px;">
								<div class="col-sm-offset-2 col-sm-10">
									<a class="btn btn-sm btn-primary" id="save">
										<i class="icon-ok"></i>保存
									</a>
								</div>
							  </div>
							</form>
					  </div>
					</div>
				</div>
			</div>
		</div>
<div class="popover" style="z-index:9999;width:150px;position:absolute;left:45%;"><div class="arrow"></div><div class="popover-inner"><h3 class="popover-title">操作提示</h3><div class="popover-content" id="popover-content"><p>操作成功</p></div></div></div>

<div id="loading" style="width:32px;height:32px;position:absolute;left:40%;top:40%;display:none;">
	<img src="<%=contextPath%>/admin/assets//css/images/loading.gif" width="32" height="32" />
</div>
		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='../assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script type="text/javascript">
			window.jQuery || document.write("<script src='../assets/js/jquery-2.0.3.min.js'>"+"<"+"script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
		<script type="text/javascript">
		window.jQuery || document.write("<script src='../assets/js/jquery-1.10.2.min.js'>"+"<"+"script>");
		</script>
		<![endif]-->
		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='../assets/js/jquery.mobile.custom.min.js'>"+"<"+"script>");
		</script>
		<script src="../assets/js/bootstrap.min.js"></script>
		<script src="../assets/js/typeahead-bs2.min.js"></script>

		<!-- page specific plugin scripts -->

		<!--[if lte IE 8]>
		  <script src="../assets/js/excanvas.min.js"></script>
		<![endif]-->

		<script src="../assets/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="../assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="../assets/js/jquery.slimscroll.min.js"></script>
		<script src="../assets/js/jquery.easy-pie-chart.min.js"></script>
		<script src="../assets/js/jquery.sparkline.min.js"></script>
		<script src="../assets/js/fuelux/fuelux.spinner.min.js"></script>
		<script src="../assets/js/date-time/bootstrap-datepicker.min.js"></script>
		<script src="../assets/js/ace-elements.min.js"></script>
		<script src="../assets/js/ace.min.js"></script>
		<!-- inline scripts related to this page -->
		<script src="../assets/js/jquery.dataTables.bootstrap.js"></script>
		<script src="../assets/js/jquery-form.js"></script>
		<script src="../js/config.js"></script>
		<script src="../js/common.js"></script>
		<script src="../js/city.js"></script>
		<script type="text/javascript" src="../ckeditor/ckeditor.js"></script> 
		
		<script>
			init();
			function init(){
				if(action == "edit"){
					bindOldData();
				}
				$("#save").bind("click",function(){
					if(action == "add"){
						save();
					}else{
						edit();
					}
					
				});
			}
			
			function bindOldData(){
				$.ajax({
					url : contextPath + '/admin/version/get.do',
					type : "POST",
					dataType : 'json',
					data : {
						id:id
					},
					beforeSend : function() {
						showLoader();
					},
					complete : function() {
						hideLoader();
					},
					success : function(rs, textStatus, jqXHR) {
						if (rs.result == "success") {
							var data = rs.data;
							$("#id").val(data.id);
							$("#version").val(data.version);
							$("#content").val(data.content);
							$("#url").val(data.url);
							$(".platform")[data.platform].checked = "checked";
							$(".isRelease")[data.rel].checked = "checked";
						} else {
							showMessage("获取数据失败,请稍后再试");
						}

					},
					error : function() {
						showMessage("请稍后再试");
					}
				});		
			}
			
			
			function save(){
				if(validate() == 1){
					return;
				}
				$("#loading").show();
				$("#versionForm").ajaxSubmit({
					type: "POST",
					url: contextPath + '/admin/version/add.do',
					dataType:'json',
					success: function (data) {
						if(data.result == "success"){
							window.location.href="versionList.jsp";
						}else{
							showMessage("添加失败");
						}
						$("#loading").hide();
					},
					error: function (msg) {
						$("#loading").hide();
						showMessage("添加失败");
					}
				});
			}
			
			
			function edit(){
				if(validate() == 1){
					return;
				}
				$("#loading").show();
				$("#versionForm").ajaxSubmit({
					type: "POST",
					url: contextPath + '/admin/version/update.do',
					dataType:'json',
					success: function (data) {
						if(data.result == "success"){
							showMessage("更新成功");
							bindOldData();
						}else{
							showMessage("更新失败");
						}
						$("#loading").hide();
					},
					error: function (msg) {
						$("#loading").hide();
						showMessage("更新失败");
					}
				});
			}
			
			function validate(){
				var version = $("#version");
				if(version.val() == ""){
					showMessage("请输入版本编号!!");
					return 1;
				}
				var content = $("#content");
				if(content.val() == ""){
					showMessage("请输入更新内容!!");
					return 1;
				}
				var file = $("#file");
				var url = $("#url");
				if(file.val() == ""){
					if(url.val() == ""){
						showMessage("请输入下载地址!!");
						return 1;
					}
				}else{
					url.val("");
				}
			}
			
		</script>
	</body>
</html>

