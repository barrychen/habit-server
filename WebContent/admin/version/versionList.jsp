﻿<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cyou.fz.common.web.WebContext"%>
<%@page import="com.cyou.fz.pay.business.po.AdminUserPO"%>
<%@page import="com.cyou.fz.pay.business.constant.SessionKeyConstant"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
	String contextPath = request.getContextPath();
	AdminUserPO adminUser = (AdminUserPO) WebContext
			.getSessionAttribute(SessionKeyConstant.ADMIN_LOGIN_USER);
	if(adminUser == null){
		// 未登录
		response.sendRedirect(contextPath + "/login.jsp?state=" + "session-invalid");
		return;
	}
	String realName = (String) WebContext
			.getSessionAttribute(SessionKeyConstant.ADMIN_LOGIN_USER_ACCOUNT);
	Long adminId = adminUser.getId();
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>App版本管理</title>
<link href="../css/base.css" rel="stylesheet" />
<link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="../assets/css/font-awesome.min.css" />

<!--[if IE 7]>
		  <link rel="stylesheet" href="../assets/css/font-awesome-ie7.min.css" />
		<![endif]-->



<link rel="stylesheet" href="../assets/css/ace.min.css" />
<link rel="stylesheet" href="../assets/css/ace-rtl.min.css" />
<link rel="stylesheet" href="../assets/css/ace-skins.min.css" />

<!--[if lte IE 8]>
		  <link rel="stylesheet" href="../assets/css/ace-ie.min.css" />
		<![endif]-->



<script src="../assets/js/ace-extra.min.js"></script>


<!--[if lt IE 9]>
		<script src="../assets/js/html5shiv.js"></script>
		<script src="../assets/js/respond.min.js"></script>
		<![endif]-->
<script type="text/javascript">  
       		var contextPath='<%=contextPath%>';
</script>
<link rel="stylesheet"
	href="../assets/css/jquery-ui-1.10.3.custom.min.css" />
<link rel="stylesheet" href="../assets/css/chosen.css" />
<style type="">
	.cutshow{text-decoration: none;}
	.cutshow:hover{text-decoration: none}
</style>
</head>
<body style="background-color: #fff;">
	<div class="breadcrumbs" id="breadcrumbs">
		<script type="text/javascript">
			try {
				ace.settings.check('breadcrumbs', 'fixed')
			} catch (e) {
			}
		</script>
		<ul class="breadcrumb">
			<li><i class="icon-home home-icon"></i> <a href="../index.jsp">首页</a>
			</li>
			<li class="active">App版本管理</li>
		</ul>
		<!-- .breadcrumb -->
	</div>
	<div class="page-content">
		<div class="page-header">
			<h1>
				App版本管理 <small> <i class="icon-double-angle-right"> 版本列表 </i>
				</small>
			</h1>
		</div>
		<!-- /.page-header -->
		<div class="row" style="height: 100%">

			<div class="col-xs-12">
				<div class="panel panel-default">
					<div class="panel-heading" style="padding: 0px;">
						<span class="icon"> <i class="icon-search"></i>
						</span>
						<h5>版本搜索</h5>
						<button type="button" onClick="expand(this)"
							class="func_btn pull-right">
							<i class="icon-refresh"></i> 展开/关闭
						</button>
					</div>
					<div class="panel-body" style="background-color: #F9F9F9;">
						<form class="form-inline" name="myForm" style="margin: 0px">
							 <div class="form-group">
								<label>平台：</label>
								<select	id="platform" class="form-control" style="width:140px;border-radius: 4px !important;">
									<option value="-1" selected>
										请选择平台
									</option>
									<option value="0">
										Android
									</option>
									<option value="1">
										ISO
									</option>
								</select>
							  </div>
							  <div class="form-group">
								<label>是否发布：</label>
								<select	id="isRelease" class="form-control" style="width:140px;border-radius: 4px !important;">
									<option value="-1" selected>
										请选择状态
									</option>
									<option value="0">
										发布
									</option>
									<option value="1">
										未发布
									</option>
								</select>
							  </div>
							<div class="form-group">
								<button type="button" class="func_btn" id="Search"
									onclick="search()">
									<i class="icon-search"></i> 搜 索
								</button>
							</div>
						</form>

					</div>
				</div>
				<div style="margin-bottom: 10px;">
					<button type="button" class="btn btn-primary btn-sm" id="add">
						<i class="icon-plus"></i>添加版本
					</button>
				</div>
				<div class="table-responsive">
					<input type="hidden" value="1" id="start" />
					<input type="hidden" value="" id="property" />
					<table id="sample-table-2"
						class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th class="center"><label> <input type="checkbox"
										class="ace" /> <span class="lbl"></span>
								</label></th>
								<th>平台信息</th>
								<th>版本</th>
								<th>更新内容</th>
								<th>下载地址</th>
								<th>是否发布</th>
								<th>操作</th>
							</tr>
						</thead>

						<tbody id="data-list">

						</tbody>
					</table>
				</div>
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
		<div class="row">
			<div class="col-sm-6">
				<div id="sample-table-2_info" class="dataTables_info">
					当前 <span id="current"></span> 到 <span id="page"></span> 总共 <span
						id="total"></span> 条
				</div>
			</div>
			<div class="col-sm-6">
				<div class="dataTables_paginate paging_bootstrap">
					<ul class="pagination pagination-sm" id="page-list">

					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="popover"
		style="z-index: 9999; width: 150px; position: absolute; left: 45%;">
		<div class="arrow"></div>
		<div class="popover-inner">
			<h3 class="popover-title" >操作提示</h3>
			<div class="popover-content" id="popover-content">
				<p>操作成功</p>
			</div>
		</div>
	</div>

	<div id="loading"
		style="width: 32px; height: 32px; position: absolute; left: 40%; top: 40%; display: none;">
		<img src="<%=contextPath%>/admin/assets//css/images/loading.gif"
			width="32" height="32" />
	</div>
	<script type="text/javascript">
		if ("ontouchend" in document)
			document
					.write("<script src='../assets/js/jquery.mobile.custom.min.js'>"
							+ "<"+"/script>");
	</script>
	<script type="text/javascript">
		window.jQuery
				|| document
						.write("<script src='../assets/js/jquery-2.0.3.min.js'>"
								+ "<"+"script>");
	</script>

	<!-- <![endif]-->

	<!--[if IE]>
		<script type="text/javascript">
		window.jQuery || document.write("<script src='../assets/js/jquery-1.10.2.min.js'>"+"<"+"script>");
		</script>
		<![endif]-->
	<script type="text/javascript">
		if ("ontouchend" in document)
			document
					.write("<script src='../assets/js/jquery.mobile.custom.min.js'>"
							+ "<"+"script>");
	</script>
	<script src="../assets/js/bootstrap.min.js"></script>
	<script src="../assets/js/typeahead-bs2.min.js"></script>

	<!-- page specific plugin scripts -->

	<!--[if lte IE 8]>
		  <script src="../assets/js/excanvas.min.js"></script>
		<![endif]-->

	<script src="../assets/js/jquery-ui-1.10.3.custom.min.js"></script>
	<script src="../assets/js/jquery.ui.touch-punch.min.js"></script>
	<script src="../assets/js/jquery.slimscroll.min.js"></script>
	<script src="../assets/js/jquery.easy-pie-chart.min.js"></script>
	<script src="../assets/js/jquery.sparkline.min.js"></script>
	<script src="../assets/js/fuelux/fuelux.spinner.min.js"></script>
	<!-- ace scripts -->
	<script src="../assets/js/ace-elements.min.js"></script>
	<script src="../assets/js/ace.min.js"></script>
	<!-- inline scripts related to this page -->
	<script src="../assets/js/jquery.dataTables.bootstrap.js"></script>
	<script src="../js/config.js"></script>
	<script src="../js/common.js"></script>
	<script src="../js/city.js"></script>
	<script src="../assets/js/jquery-form.js"></script>
	<script type="text/javascript">
		init();
		function init() {
			getData(1);
		}
		function getData(start) {
			$("#start").val(start);
			$.ajax({
				url : contextPath + '/admin/version/list.do' + ($("#property").val() == "" ? "" : '?' + $("#property").val()),
				type : "POST",
				dataType : 'json',
				data : {
					pageNo : (start - 1) * pageSize,
					pageSize : pageSize
				},
				beforeSend : function() {
					showLoader();
				},
				complete : function() {
					hideLoader();
				},
				success : function(rs, textStatus, jqXHR) {
					if (rs.result == "success") {
						var data = rs.data;
						var total = data.totalCount;
						var root = data.dataList;
						var dataList = $("#data-list");
						dataList.empty();
						$("#total").text(total);
						$("#current").text((start - 1) * pageSize + 1);
						if (root.length > 0) {
							$("#page").text(
									(start - 1) * pageSize + root.length);
							parseData(root, total, start);
						} else {
							showMessage("无数据");
						}
						
					} else {
						showMessage("获取数据失败,请稍后再试");
					}

				},
				error : function() {
					$("#loading").hide();
					showMessage("请稍后再试");
				}
			});
		}

		var isRelease = [  '<span class="label label-success">发布</span>', '<span class="label label-default">未发布</span>'];
		var platform = ['Android','ISO'];
		function parseData(data, total, start) {
			var dataList = $("#data-list");
			var tr = "";
			for (var i = 0; i < data.length; i++) {
				tr += '<tr><td class="center"><label><input type="checkbox" value="'+data[i].id+'" class="ace" /><span class="lbl"></span></label></td>';
				tr += '<td>' + platform[data[i].platform] + '</td>';
				tr += '<td>' + data[i].version + '</td>';
				tr += '<td>' + data[i].content + '</td>';
				tr += '<td>' + data[i].url + '</td>';
				tr += '<td>' + isRelease[data[i].rel] + '</td>';
				tr += '<td><div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">';
				if(data[i].rel == 0){
					tr += '<label><input name="switch-field-1" class="ace ace-switch unShow" checked="checked"  type="checkbox" style="margin-top:5px;" /><span class="lbl"></span></label>'
				}else{
					tr += '<label><input name="switch-field-1" class="ace ace-switch isShow" type="checkbox" style="margin-top:5px;" /><span class="lbl"></span></label>'
				}
				tr += '&nbsp;<input type="hidden" value="'+data[i].id+'" /><button type="button" style="padding:0px;" class="btn btn-success edit" >修改</button>&nbsp;';
				tr += '</div></td>';
				tr += '</tr>';
			}
			dataList.append(tr);
			//分页
			var pageList = $("#page-list");
			pageList.empty();
			pageList.append(getPageList(start, total));
			$(".unShow").bind("click",function(){
				var tr = $(this).parent().parent().parent().parent();
				var id = tr.find("input").val();
				changeShow(id,1);
			});
			$(".isShow").bind("click",function(){
				var tr = $(this).parent().parent().parent().parent();
				var id = tr.find("input").val();
				changeShow(id,0);
			});
			$(".edit").bind("click",function(){
				var id = $(this).prev().val();
				window.location.href="versionEdit.jsp?id=" + id;
			});
		}
		
		function search() {
			var property = "";
			var platform = $("#platform").val();
			if(platform != -1 ) {
				property += "platform=" + platform + "&";
			}
			var rel = $("#isRelease").val();
			if(rel != -1){
				property += 'rel=' + rel + "&";
			}
			if(property != ""){
				property = property.substring(0,property.length -1 );
				$("#property").val(property);
			}else{
				$("#property").val("");
			}
			getData(1);
		}
		
		$("#add").bind("click",function(){
			window.location.href='versionEdit.jsp';
		});
		
		function changeShow(id,isShow){
			$.ajax({
				url : contextPath + '/admin/version/setRelease.do?id='+id+'&isRelease=' + isShow,
				type : "POST",
				dataType : 'json',
				beforeSend : function() {
					showLoader();
				},
				complete : function() {
					hideLoader();
				},
				success : function(rs, textStatus, jqXHR) {
					if (rs.result == "success") {
						showMessage("操作成功");
						getData($("#start").val());
					} else {
						showMessage("获取数据失败,请稍后再试");
					}

				},
				error : function() {
					$("#loading").hide();
					showMessage("请稍后再试");
				}
			});
		}
	</script>
</body>
</html>