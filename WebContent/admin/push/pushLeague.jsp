<%@ page language="java" pageEncoding="UTF-8"%> 
<%@page import="com.cyou.fz.common.web.WebContext"%>
<%@page import="com.cyou.fz.pay.business.po.AdminUserPO"%>
<%@page import="com.cyou.fz.pay.business.constant.SessionKeyConstant"%>
<%
String contextPath = request.getContextPath();
AdminUserPO adminUser = (AdminUserPO) WebContext
.getSessionAttribute(SessionKeyConstant.ADMIN_LOGIN_USER);
if(adminUser == null){
	response.sendRedirect(contextPath + "/login.jsp?state=" + "session-invalid");
	return;
}
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+contextPath+"/";
String leagueId = request.getParameter("id");
if(leagueId == null){
	leagueId = "";
}
%>
<%@page import="java.util.UUID"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
		<link href="../css/base.css" rel="stylesheet" />
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="../assets/css/font-awesome.min.css" />

		<!--[if IE 7]>
		  <link rel="stylesheet" href="../assets/css/font-awesome-ie7.min.css" />
		<![endif]-->

		

		<link rel="stylesheet" href="../assets/css/ace.min.css" />
		<link rel="stylesheet" href="../assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="../assets/css/ace-skins.min.css" />
		<link rel="stylesheet" href="../assets/css/datepicker.css" />
		<link rel="stylesheet" href="../assets/css/bootstrap-timepicker.css" />
		<link rel="stylesheet" href="../css/base.css" />
		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="../assets/css/ace-ie.min.css" />
		<![endif]-->

		

		<script src="../assets/js/ace-extra.min.js"></script>


		<!--[if lt IE 9]>
		<script src="../assets/js/html5shiv.js"></script>
		<script src="../assets/js/respond.min.js"></script>
		<![endif]-->
       	<script type="text/javascript">  
       		var contextPath='<%=contextPath%>';
       		
       		var leagueId = '<%=leagueId%>';
		</script>
		<link rel="stylesheet" href="../assets/css/jquery-ui-1.10.3.custom.min.css" />
		<link rel="stylesheet" href="../assets/css/chosen.css" />
	</head>
	<body style="padding:0;margin:0;background-color:#fff;">
		<div class="breadcrumbs" id="breadcrumbs">
			<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="../index.jsp">首页</a>
							</li>
							<li class="active">赛事推送</li>
						</ul>
		</div>
		
		<div class="container-fluid" style="padding:0 20px;font-size:12px;margin-top:20px;">
			<div class="row-fluid">
				<div class="span12">
					<div class="panel panel-default">
					  <div class="panel-heading">
							<span class="icon">
								<i class="icon-plus"></i>
							</span>
							<h5>赛事推送</h5>
					  </div>
					  <div class="panel-body" style="padding:0px;background-color:#F9F9F9;">
							<form class="form-horizontal" style="padding-top:10px;" method="post" id="pushForm">
							  <input type="hidden" value="<%=leagueId%>" id="leagueId" name="leagueId" />
							  <div class="form-group">
								<label for="" class="col-sm-2 control-label">标题：</label>
								<div class="col-sm-8">
								  <input id="title" class="form-control" maxlength="50" type="text" name="title" value=""/>
								</div>
								<span class="input_tip" style="line-height:30px;">*</span>
							  </div>
							  
							  <div class="form-group">
								<label for="" class="col-sm-2 control-label">消息：</label>
								<div class="col-sm-8">
								  <textarea rows="8" name="message" id="message" class="form-control"></textarea>
								</div>
								<span class="input_tip" style="line-height:30px;">*</span>
							  </div>
							  
							  <div class="form-group">
								<label for="" class="col-sm-2 control-label">推送对象：</label>
								<div class="col-sm-8" >
								  <input type="radio" value="all" class="sendTarget" name="sendTarget" checked/>&nbsp;广播(所有人) &nbsp;
								  <input type="radio" value="tag" class="sendTarget" name="sendTarget" />&nbsp;设备标签(Tag)&nbsp;
								  <input type="radio" value="alias" class="sendTarget" name="sendTarget" />&nbsp;设备别名(Alias)&nbsp;
								  <input type="radio" value="registrationId" class="sendTarget" name="sendTarget" />&nbsp;Registration ID<br/><br/>
								  <input class="form-control" style="display:none;" maxlength="50" type="text" name="targetValue" id="targetValue" value=""/>
								</div>
							  </div>
							  
							  <div class="form-group">
								<label for="" class="col-sm-2 control-label">发送时间：</label>
								<div class="col-sm-8" >
								  <input type="radio" value="immediately" class="sendTime" name="sendTime" checked/>&nbsp;立即 &nbsp;
								  <input type="radio" value="alam" class="sendTime" name="sendTime" />&nbsp;定时&nbsp;
								  <input type="radio" value="speed" class="sendTime" name="sendTime" />&nbsp;定速推送&nbsp;<br/><br/>
								  <input type="hidden" value="" id="timeValue" name="timeValue" />
								  <div id="setAlam" style="display:none">
								  	<input class="form-control" style="width:150px;float:left;" type="text" id="date" readOnly="readOnly" />
									<span class="input-group-addon" style="width:40px;border-radius: 4px !important;float:left">
										<i class="icon-calendar bigger-110"></i>
									</span>
									<div class="input-group bootstrap-timepicker" style="width:150px;">
															<input id="time1" type="text" class="form-control" />
															<span class="input-group-addon">
																<i class="icon-time bigger-110"></i>
															</span>
														</div>
									</div>
								  <div id="setSpeed" style="display:none">推送将分布在  <input  class="form-control" placeholder="不能超过1440" style="width:200px;display:inline;" maxlength="50" type="text" id="speedValue" value=""/> 分钟内完成</div>
								</div>
							  </div>
							  
							  <div class="form-group">
								<label for="" class="col-sm-2 control-label">离线消息保留时长：</label>
								<div class="col-sm-8" >
								  	<select class="form-control" id="keepTime" name="keepTime" style="width:200px;float:left;">
										<option value="86400" selected="selected">默认(1天)</option>
										<option value="0">0 - 不保留</option>
										<option value="60">1 分钟</option>
										<option value="600">10 分钟</option>
										<option value="3600">1 小时</option>
										<option value="10800">3 小时</option>
										<option value="43200">12 小时</option>
										<option value="259200">3 天</option>
										<option value="864000">10 天</option>
										<option value="-1">自定义</option>
									</select>
									<input  class="form-control" placeholder="单位是秒" style="width:200px;display:none;float:left;" maxlength="50" type="text" name="keepValue" id="keepValue" value=""/>
								</div>
							  </div>
							  
							  <div class="space-4"></div>
							  <div class="form-group" style="padding-top:10px;">
								<div class="col-sm-offset-2 col-sm-10">
									<a class="btn btn-sm btn-primary" id="save">
										<i class="icon-ok"></i>保存
									</a>
								</div>
							  </div>
							</form>
					  </div>
					</div>
				</div>
			</div>
		</div>
<div class="popover" style="z-index:9999;width:150px;position:absolute;left:45%;"><div class="arrow"></div><div class="popover-inner"><h3 class="popover-title">操作提示</h3><div class="popover-content" id="popover-content"><p>操作成功</p></div></div></div>

<div id="loading" style="width:32px;height:32px;position:absolute;left:40%;top:40%;display:none;">
	<img src="<%=contextPath%>/admin/assets//css/images/loading.gif" width="32" height="32" />
</div>
		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='../assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script type="text/javascript">
			window.jQuery || document.write("<script src='../assets/js/jquery-2.0.3.min.js'>"+"<"+"script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
		<script type="text/javascript">
		window.jQuery || document.write("<script src='../assets/js/jquery-1.10.2.min.js'>"+"<"+"script>");
		</script>
		<![endif]-->
		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='../assets/js/jquery.mobile.custom.min.js'>"+"<"+"script>");
		</script>
		<script src="../assets/js/bootstrap.min.js"></script>
		<script src="../assets/js/typeahead-bs2.min.js"></script>

		<!-- page specific plugin scripts -->

		<!--[if lte IE 8]>
		  <script src="../assets/js/excanvas.min.js"></script>
		<![endif]-->

		<script src="../assets/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="../assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="../assets/js/jquery.slimscroll.min.js"></script>
		<script src="../assets/js/jquery.easy-pie-chart.min.js"></script>
		<script src="../assets/js/jquery.sparkline.min.js"></script>
		<script src="../assets/js/fuelux/fuelux.spinner.min.js"></script>
		<script src="../assets/js/date-time/bootstrap-datepicker.min.js"></script>
		<script src="../assets/js/date-time/bootstrap-timepicker.min.js"></script>
		<script src="../assets/js/ace-elements.min.js"></script>
		<script src="../assets/js/ace.min.js"></script>
		<!-- inline scripts related to this page -->
		<script src="../assets/js/jquery.dataTables.bootstrap.js"></script>
		<script src="../assets/js/jquery-form.js"></script>
		<script src="../js/config.js"></script>
		<script src="../js/common.js"></script>
		<script src="../js/city.js"></script>
		<script type="text/javascript" src="../ckeditor/ckeditor.js"></script> 
		
		<script>
			init();
			function init(){
				$("#date").datepicker({format: 'yyyy-mm-dd',autoclose:true}).next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
				$("#date").val(parseDate(new Date().valueOf()));
				$("#time1").timepicker({
					minuteStep: 1,
					showSeconds: true,
					showMeridian: false
				}).next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
				$("#keepTime").bind("change",function(){
					var val = $(this).val();
					var keepValue = $("#keepValue");
					if(val == -1){
						keepValue.show();
						keepValue.val("");
					}else{
						keepValue.val($(this).val());
						keepValue.hide();
					}
				});
				$(".sendTarget").bind("change",function(){
					var val = $(this).val();
					var targetValue = $("#targetValue");
					if(val == "all"){
						targetValue.hide();
						targetValue.val("");
					}else if(val == "tag"){
						targetValue.show();
						targetValue.val("");
						targetValue.attr("placeholder","请输入设备标签(多个以逗号分割)");
					}else if(val == "alias"){
						targetValue.show();
						targetValue.val("");
						targetValue.attr("placeholder","请输入设备别名(多个以逗号分割)");
					}else if(val == "registrationId"){
						targetValue.show();
						targetValue.val("");
						targetValue.attr("placeholder","请输入Registration ID(多个以逗号分割)");
					}
				});
				
				$(".sendTime").bind("change",function(){
					var val = $(this).val();
					var setAlam = $("#setAlam");
					var setSpeed = $("#setSpeed");
					if(val == "immediately"){
						setAlam.hide();
						setSpeed.hide();
					}else if(val == "alam"){
						setAlam.show();
						setSpeed.hide();
					}else if(val == "speed"){
						setAlam.hide();
						setSpeed.show();
					}
				});
				$("#save").bind("click",function(){
						save();
				});
				if(leagueId != ""){
					$.ajax({
						url : contextPath + '/admin/league/get.do',
						type : "POST",
						dataType : 'json',
						data : {
							id:leagueId
						},
						beforeSend : function() {
							showLoader();
						},
						complete : function() {
							hideLoader();
						},
						success : function(rs, textStatus, jqXHR) {
							if (rs.result == "success") {
								var data = rs.data;
								$("#title").val(data.title);
							} else {
								showMessage("获取数据失败,请稍后再试");
							}

						},
						error : function() {
							showMessage("请稍后再试");
						}
					});	
				}
			}
			
			
			
			function save(){
				if(validate() == 1){
					return;
				}
				var val = $(".sendTarget:checked").val();
				var targetValue = $("#targetValue");
				if(val == "all"){
					
				}else if(val == "tag"){
					if(targetValue.val() == ""){
						showMessage("请输入设备标签(多个以逗号分割)");
						return;
					}
				}else if(val == "alias"){
					if(targetValue.val() == ""){
						showMessage("请输入设备别名(多个以逗号分割)");
						return;
					}
				}else if(val == "registrationId"){
					if(targetValue.val() == ""){
						showMessage("请输入Registration ID(多个以逗号分割)");
						return;
					};
				}
				val = $(".sendTime:checked").val();
				var timeValue = $("#timeValue");
				if(val == "immediately"){
					timeValue.val("");
				}else if(val == "alam"){
					timeValue.val($("#date").val() + " " + $("#time1").val());
				}else if(val == "speed"){
					if($("#speedValue").val() == ""){
						showMessage("请输入定速推送分钟数");
						return;
					}
					timeValue.val($("#speedValue").val());
				}
				$("#loading").show();
				$("#pushForm").ajaxSubmit({
					type: "POST",
					url: contextPath + '/push/pushLeague.do',
					dataType:'json',
					success: function (data) {
						if (data.result == "success") {
							showMessage("推送成功");
							$("#title").val("");
							$("#message").val("");
							$("#keepTime").val("86400");
							$("#keepValue").val("");
							$("input[value='immediately']").trigger("click");
							$("input[value='all']").trigger("click");
							$("#date").val(parseDate(new Date().valueOf()));
							$("#leagueId").val("");
						} else {
							showMessage("推送失败,请稍后再试");
						}
						$("#loading").hide();
					},
					error: function (msg) {
						$("#loading").hide();
						showMessage("推送失败");
					}
				});
				
			}
			
			function validate(){
				var title = $("#title");
				if(title.val() == ""){
					showMessage("请输入通知的标题!!");
					return 1;
				}
				var message = $("#message");
				if(message.val() == ""){
					showMessage("请输入推送的消息!!");
					return 1;
				}
				
			}
			
		</script>
	</body>
</html>

