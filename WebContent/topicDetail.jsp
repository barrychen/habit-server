<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
String topicId = request.getParameter("id");
String contextPath = request.getContextPath();
%>
<html>
<head>
<meta name="viewport" content="width=device-width,initial-scale=1"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> 
<link rel="stylesheet" href="css/bootstrap3/css/bootstrap-theme.min.css" />
<link rel="stylesheet" href="css/bootstrap3/css/bootstrap.min.css" />
<link rel="stylesheet" href="css/jquery.mobile-1.4.5.min.css" />
<script src="js/jquery.min.js"></script>
<script src="js/jquery.mobile-1.4.5.min.js"></script>
<script src="css/bootstrap3/js/bootstrap.min.js"></script>
<style type="text/css">
		body{
			font:12px 微软雅黑;
		}
		#swipebox-top-bar {
			color: #FFF !important;
			font-size: 12px;
			line-height: 15px;
			top: auto;
			font-family: Helvetica,Arial,sans-serif;
		}
</style>
</head>
<body >
<div data-role="page"  id="topicDetail" style="background-color:#edebec">
<script>
var contextPath='<%=contextPath%>';
var replyListPageSize = 10;
var replyListPageName = "#topicListPage";
var replyListPageIsloaded = false;// 防止执行重复
var replyListPagePageSize = 20;
var replyListPagePageNo = 0;
var replyListPageNextpageNo = 0;
var replyListPageTotalPageCount = 0;
var replyListPageTotalCount = 0;

var topicId = '<%=topicId%>';

$(document).on("pageinit", "#topicDetail", function(event) {
	//获取主题信息
	topicDetailGetAjaxData(topicId);
	$(document).unbind("scroll");
	//replyListScrollBind();
});


function replyListScrollBind() {
	$(document).bind(
			"scroll",
			function(event) {
				if ($(document).scrollTop() >= ($(document).height()- $(window).height() - 50)) {
					if (replyListPagePageNo < replyListPageTotalPageCount) {
						$(document).unbind("scroll");
						topicReplyGetAjaxData(false);
					}

				}
			});
}

function topicDetailGetAjaxData(topicId) {
	$("#topic").empty();
	$.ajax({
				type : "POST",
				dataType : "json",// 返回json格式的数据
				url : contextPath + "/topic/get.do?id=" + topicId,
				success : function(json) {
					if (json.result == "success") {
						$("#topic").empty();
						var data = json.data;
						var html = "";
						html += '<div class="col-xs-12"><div style="text-align:left;margin-bottom:5px;margin-top:5px">';
							
						html += '<img src="'+contextPath + "/" + data.logo+'" class="playerLogo" />';
						
						html += '<div style="height:55px;"><div class="nickname">'+data.nickname+'</div>';
						
						html += '<div class="createDate">'+data.creatDateStr+'</div></div></div>';

						html += '<div><img src="'+contextPath + "/" + data.pic+'" id="pic" width="100%" /></div>';
						
						html += '<div class="des">'+data.des+'</div></div>';

						$("#topic").append(html);
						//获取主题回复
						topicReplyGetAjaxData(true);
					}
				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {
					alert("异常信息：" + textStatus);
				}
			});
}

function topicReplyGetAjaxData(isempty) {
	if(isempty){
		$("#replyList").empty();
		topicListPagePageNo=0;
	}
	$.ajax({
				type : "POST",
				data : {
					"pageNo" : replyListPageNextpageNo,
					"pageSize" : replyListPageSize,
					"refId" : topicId,
					"refType":0
				},
				dataType : "json",// 返回json格式的数据
				url : contextPath + "/reply/list.do",
				success : function(json) {
					if (json.result == "success") {
						var data = json.data;
						replyListPageTotalCount = data.totalCount;
						replyListPageTotalPageCount = data.totalPageCount;
						replyListPageNextpageNo = replyListPageNextpageNo
								+ replyListPageSize;
						replyListPagePageNo = replyListPagePageNo + 1;
						

						var dataList = data.dataList;
						var len = dataList.length;
						var html = "";
						for ( var key in dataList) {
							html += '<div class="col-xs-12 replyTemplet" replyId="'+dataList[key].id+'">';
							html += '<div style="text-align:left;margin-top:5px;"><img src="'+contextPath + "/" + dataList[key].logo+'" class="playerLogo" />';
							html += '<div style="height:55px;width:80%"><p class="nickname">'+dataList[key].nickname +'</p>';
							html += '<div class="content"><p class="replyCreateDate">'+dataList[key].creatDateStr+'</p></div></div></div>';
							html += '<div style="word-wrap:break-word;text-align:left;margin-bottom:10px;padding-left:70px">'+dataList[key].content+'</div>';
							html += '</div>';
						}
						$("#replyList").append(html);
						if (replyListPagePageNo < replyListPageTotalPageCount) {
							replyListScrollBind();
						}

					}
				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {
					alert("异常信息：" + textStatus);
				}
			});
}

	function gotoReply(){
		var replyContent = $("#reply").val();
		if(replyContent == ""){
			alert("请输入回复内容");
			return;
		}
		if(replyContent.length>20){
			alert("你写的太多了。。。");
			return;
		}
		$.ajax({
			dataType : "json",// 返回json格式的数据
			type:"POST",
			data : {
				"content" : replyContent,
				"topicId" : topicId,
				"playerId" : playerId,
				"refType" : 0,
			},
			url : contextPath + "/reply/add.do",
			success : function(json) {
				if (json.result == "success") {
					alert("评论成功");
					$("#reply").val("");
					replyListPagePageNo = 0;
					replyListPageNextpageNo = 0;
					replyListPageTotalPageCount = 0;
					replyListPageTotalCount = 0;
					topicReplyGetAjaxData(true);
				}
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert("异常信息：" + textStatus);
			}
		});	
	}
</script>
<link rel="stylesheet" href="css/customTheme/jquery.mobile.icons.min.css?2345" />
<link rel="stylesheet" href="css/customTheme/bbO2O.min.css" />
<link rel="stylesheet" href="css/login/reset.css" />
<style>
	 .leagueNotice{
			background-color:#ffffff;  
			padding: 15px;
			color:#aaaaaa;
			font-weight:bold;
			border-width:0px 0px 1px 0px; 
			border-color:#f9f8f8;
			margin-bottom: 20px;
			border: 1px solid #dddddd;
		}
	 .dateLimit{
	 	color:#ffa113;
	 	font-size:0.9em;
	 }
	 .leagueDate{
	 	color:#aaaaaa;
	 	font-size:0.9em;
	 }
	 .addBtn{
	 	width:80%;
		text-align:center;
	 	height:30px;
	 	background-color:#eb413d;
		color:#FFFFFF;
		border-radius:5px;
		margin:12.5px auto;
		line-height:30px;
	 }
	 .textSt{
	 	font:1em 微软雅黑;
	 	text-shadow:0px #000000; 
	 }
	.topicTemplet{background-color:#edebec;}
	.playerLogo{width:50px;height:50px;border-radius:50%;border:2px solid #ffffff;float:left;margin:0px 10px 0px 10px;}
	.nickname{padding:0px 0px;line-height:25px;color:#eb413d}
	.createDate{color:#A1A1A1;font-size:0.8em;line-height:25px}
	.replyCreateDate{color:#A1A1A1;font-size:0.8em;line-height:25px;text-align:left}
	.des{padding:10px 5px 10px 5px;text-align:left}
	.replyContent{width:90%;border:1px solid；padding-bottom:5px}
	.replyTemplet{border-top:1px solid #A9A9A9;background-color:#fff;}
 </style>
	<div data-role="header" style="color:#ffffff;background-color:#eb413d;border-width:0px 0px 1px 0px; border-color:#ffffff">
		<h1 >帖子评论</h1>
	</div>
	<div class="container">
		<div class="row topicTemplet" id="topic" >
		</div>	
		<div class="row" style="text-align:center;background-color:#edebec;" id="replyList">
		</div>
	</div>
	
</div>
</body >
</html>