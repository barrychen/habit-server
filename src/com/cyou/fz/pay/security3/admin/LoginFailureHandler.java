package com.cyou.fz.pay.security3.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.ExceptionMappingAuthenticationFailureHandler;

import com.cyou.fz.common.web.WebContext;
/**
 * 登录失败后业务逻辑.
 * 
 * @author yangz
 * @date 2013-4-8 下午5:34:53
 */
public class LoginFailureHandler extends ExceptionMappingAuthenticationFailureHandler{

	@Override
	public void onAuthenticationFailure(HttpServletRequest request,
			HttpServletResponse response, AuthenticationException exception)
			throws IOException, ServletException {
		WebContext.putRequestAttribute("LOGIN_ERROR_MESSAGE", "用户名密码错误.");
		super.onAuthenticationFailure(request, response, exception);
	}

	
	
}
