package com.cyou.fz.pay.security3;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;

import com.cyou.fz.common.ajax.Response;
import com.cyou.fz.common.utils.Jackson2Util;
import com.cyou.fz.common.utils.WebUtil;
/**
 * security访问无权限地址异常跳转点.
 * @Company : cyou
 * @author yangz
 * @date   2012-11-1 上午01:41:09
 */
@SuppressWarnings("deprecation")
public class MultiRequestAuthenticationEntryPoint extends LoginUrlAuthenticationEntryPoint {

	private static final Log logger = LogFactory.getLog(MultiRequestAuthenticationEntryPoint.class);

	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

	public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException)
			throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		String redirectUrl = null;
		String url = request.getRequestURI();
		if (logger.isDebugEnabled()) {
			logger.debug("url:" + url);
		}
		if (!WebUtil.isAjaxRequest(request)) {// 非ajax请求
			if (this.isUseForward()) {
				if (this.isForceHttps() && "http".equals(request.getScheme())) {
					// First redirect the current request to HTTPS.
					// When that request is received, the forward to the login page will be used.
					redirectUrl = buildHttpsRedirectUrlForRequest(httpRequest);
				}
				if (redirectUrl == null) {
					String loginForm = determineUrlToUseForThisRequest(httpRequest, httpResponse, authException);

					if (logger.isDebugEnabled()) {
						logger.debug("Server side forward to: " + loginForm);
					}
					RequestDispatcher dispatcher = httpRequest.getRequestDispatcher(loginForm);
					dispatcher.forward(request, response);
					return;
				}
			} else {
				// redirect to login page. Use https if forceHttps true
				redirectUrl = buildRedirectUrlToLoginPage(httpRequest, httpResponse, authException);
			}
			redirectStrategy.sendRedirect(httpRequest, httpResponse, redirectUrl);
		} else {//ajax请求
			Response<Boolean> responseData = new Response<Boolean>();
			responseData.setResult(Response.RESULT_LOGIN); //访问被拒绝,请求登录.
			responseData.setMessage("request refused.");
			Jackson2Util.writeJson(httpResponse, responseData);
		}
	}

}