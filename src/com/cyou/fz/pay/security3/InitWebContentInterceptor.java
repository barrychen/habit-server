package com.cyou.fz.pay.security3;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.web.FilterInvocation;

import com.cyou.fz.common.exception.UnCaughtException;
import com.cyou.fz.common.web.WebContext;
/**
 * 初始化web请求容器.
 * 
 * @author yangz
 * @date 2013-4-8 下午5:37:38
 */
public class InitWebContentInterceptor implements Filter{

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		try {
			WebContext.setRequest((HttpServletRequest) request);
			WebContext.setResponse((HttpServletResponse) response);
			FilterInvocation fi = new FilterInvocation(request, response, chain);
			invoke(fi);//继续执行过滤链
		} catch (RuntimeException e) {
			throw new UnCaughtException(e);
		}finally{
			WebContext.remove();
		}
	}

	@Override
	public void destroy() {
	}

	public void invoke(FilterInvocation fi) throws IOException, ServletException {
		fi.getChain().doFilter(fi.getRequest(), fi.getResponse());
	}
}
