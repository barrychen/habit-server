package com.cyou.fz.pay.security3;

import org.springframework.security.authentication.encoding.PasswordEncoder;

import com.cyou.fz.common.exception.UnCaughtException;
import com.cyou.fz.common.utils.HexUtil;
import com.cyou.fz.common.utils.ObjectUtil;
/**
 * 登录密码校验.
 * 
 * @author yangz
 * @date 2013-4-10 上午9:32:42
 */
public class HEXPasswordEncoder implements PasswordEncoder{

	@Override
	public String encodePassword(String rawPass, Object salt) {
		try {
			return HexUtil.getEncryptedPwd(mergePassword(rawPass, salt));
		} catch (Exception e) {
			throw new UnCaughtException(e);
		}
	}

	@Override
	public boolean isPasswordValid(String encPass, String rawPass, Object salt) {
		try {
			return HexUtil.validPasswd(mergePassword(rawPass, salt), encPass);
		} catch (Exception e) {
			throw new UnCaughtException(e);
		}
	}
	/**
	 * 合并密码.
	 * @param rawPass
	 * @param salt
	 * @return
	 * @author yangz
	 * @date 2013-4-10 上午10:26:50
	 */
	public String mergePassword(String rawPass, Object salt){
		if(ObjectUtil.isEmpty(salt)){
			return rawPass;
		}else{
			return rawPass + salt;
		}
	}

}
