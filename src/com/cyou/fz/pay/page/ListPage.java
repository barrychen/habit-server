package com.cyou.fz.pay.page;

 
import java.io.Serializable;
import java.util.List;
/**
 * 
 *
 * 
 * 基于List的数据分页对象
 */

public class ListPage<T> implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1960189378734246173L;

	public static final ListPage<Object> EMPTY_PAGE = new ListPage<Object>();

    //页面记录数，等于0时显示所有
    private int currentPageSize;
    //总记录数
    private long totalCount;
    //当前页页码
    private int currentPageNo;
    //当前页记录列表
    private List<T> dataList;
    
    public ListPage() {
    }    
    /**
     * @return 返回 currentPageno。
     */
    public int getCurrentPageNo() {
        return currentPageNo;
    }
    /**
     * @param currentPageno 要设置的 currentPageno。
     */
    public void setCurrentPageNo(int currentPageNo) {
        this.currentPageNo = currentPageNo;
    }
    /**
     * @return 返回 currentPageSize。
     */
    public int getCurrentPageSize() {
        return currentPageSize;
    }
    /**
     * @param currentPageSize 要设置的 currentPageSize。
     */
    public void setCurrentPageSize(int currentPageSize) {
        this.currentPageSize = currentPageSize;
    }
    /**
     * @return 返回 dataList。
     */
    public List<T> getDataList() {
        return dataList;
    }
    /**
     * @param dataList 要设置的 dataList。
     */
    public void setDataList(List<T> dataList) {
        this.dataList = dataList;
    }
    /**
     * @return 返回 totalSize。
     */
    public long getTotalCount() {
        return totalCount;
    }
    /**
     * @param totalSize 要设置的 totalSize。
     */
    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }
    
    
    public long getTotalPageCount(){
    	if (currentPageSize == 0) {
    		return 1;
    	}
     	return (totalCount - 1) / currentPageSize + 1 ;
    }
    
    /**
     * 是否有下一页
     * @return 是否有下一页
     */
    public boolean hasNextPage() {
      return (currentPageNo < this.getTotalPageCount());
    }

    /**
     * 是否有上一页
     * @return  是否有上一页
     */
    public boolean hasPreviousPage() {
      return (currentPageNo > 1);
    }
    
    /**
     * 判断是否为空页
     * @return 是否为空页
     */
    public boolean isEmpty(){
        return totalCount == 0;
    }
    
}
