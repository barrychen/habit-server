/**
 * 北京畅游时空软件技术有限公司福州分公司 - 版权所有
 * 2013-5-14 上午9:12:06
 */
package com.cyou.fz.pay.pub.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import nl.bitwalker.useragentutils.UserAgent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cyou.fz.common.exception.GlobalExceptionHandler;

/**
 * 主要用于处理产生404的错误，因为GlobalExceptionHandler无法捕获到不存在的RequestMapping 404的错误.
 * 
 * @author zhufu
 * 
 */
@Controller
public class ErrorController {
	/**
	 * 全局异常处理.
	 */
	@Autowired
	private GlobalExceptionHandler handler;
	/**
	 * 日志.
	 */
	private static final Logger logger = LoggerFactory
			.getLogger(ErrorController.class);
	@Value("${system.contentType}")
	private String contentType;

	/**
	 * 处理发生错误时的跳转.
	 * 
	 * @param request
	 *            请求
	 * @return 页面地址
	 * @author zhufu 2013 2013-5-14 上午11:11:42 动作:新建
	 */
	@RequestMapping("/error")
	public String errorHandle(HttpServletRequest request,
			HttpServletResponse response) {
		// logger.info("进入错误控制器 in {}", System.currentTimeMillis());
		String returnUrl = handler.getFrontErrorPage();
		String frontOrAdmin = "前台";
		// 获得发生错误时的请求路径
		String uri = request.getAttribute("javax.servlet.error.request_uri")
				.toString();
		if (uri.contains("admin/")) { // 区分前后台
			frontOrAdmin = "后台";
			returnUrl = handler.getAdminErrorPage();
		} else {
			// 获得user agent
			String userAgentStr = request.getHeader("User-Agent");
			UserAgent userAgent = UserAgent.parseUserAgentString(userAgentStr);
			boolean isMobileDevice = userAgent.getOperatingSystem()
					.isMobileDevice()
					|| userAgentStr.toLowerCase().contains("pad");
			if (isMobileDevice) { // 如果是手机端返回手机端的404页面
				frontOrAdmin = "前台手机端";
				returnUrl = handler.getMobileErrorPage();
			}
		}
		// 获取异常信息 一般是ServletException
		Throwable e = (Throwable) request
				.getAttribute("javax.servlet.error.exception");
		String errorMsg = "throwable不存在，未查到错误原因，可能就是页面找不到";
		// 异常原因
		if (e != null) {
			Throwable t = e.getCause();
			if (t != null) {
				errorMsg = t.toString();
//				errorMsg = t.getMessage();
			} else {
				errorMsg = e.toString();
			}
		}
		logger.error(
				"{}发生错误http错误码为 : {},  errorMsg : {}, 请求地址 : {}",
				new Object[] {
						frontOrAdmin,
						request.getAttribute("javax.servlet.error.status_code"),
						errorMsg, uri });
		response.setContentType(contentType);
		return returnUrl;
	}
}
