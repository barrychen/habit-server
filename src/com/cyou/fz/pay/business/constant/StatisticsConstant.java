package com.cyou.fz.pay.business.constant;

/**
 * 所查询数据字段常量.
 * 
 * @author Dipin
 * @date 2013-4-22 下午10:00:59
 */
public class StatisticsConstant {
	/**
	 * 导出excel时，查询数据字段对应的中文常量.
	 */
	public static final String[] DATA_FIELD = { "支付成功金额(#{payCurrency})", "生成订单数", "支付成功订单数",
			"支付帐号数", "首次支付帐号数" };
	/**
	 * 导出excel时，dataField对应的中文常量.
	 */
	public static final String DATA_FIELD_NAME = "时间区间";
	/**
	 * 导出excel时，bpName对应的中文常量.
	 */
	public static final String BP_NAME = "所属接口业务";
}
