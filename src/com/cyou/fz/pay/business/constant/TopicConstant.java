package com.cyou.fz.pay.business.constant;

/**
 * 主题信息常量.
 * 
 */
public class TopicConstant {

	/**
	 * 用户帖子
	 */
	public final static Long TOPIC = 0L;
	/**
	 * 广告帖子
	 */
	public final static Long AD_TOPIC = 1L;

	/**
	 * 引用topic id
	 */
	public final static Long REF_TOPIC = 0L;

	/**
	 * 引用 reply id
	 */
	public final static Long REF_REPLY = 1L;

}
