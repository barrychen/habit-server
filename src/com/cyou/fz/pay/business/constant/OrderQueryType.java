package com.cyou.fz.pay.business.constant;

/**
 * 订单查询类型.
 * 
 * @author qingwu
 * @date 2013-04-23 下午02:00:00
 */
public class OrderQueryType {

	private OrderQueryType() {

	}

	/**
	 * 后台管理人员查询.
	 */
	public static final int ADMIN_QUERY_NORMAL = 1;
	/**
	 * 前台用户订单查询.
	 */
	public static final int FRONT_QUERY_NORMAL = 2;
	/**
	 * 后台EXCEL导出.
	 */
	public static final int ADMIN_EXCEL_EXPORT = 3;
	/**
	 * 前台用户等待付款订单查询.
	 */
	public static final int FRONT_QUERY_UNPAID = 4;
	/**
	 * 无规则校验.
	 */
	public static final int NO_RULE = 99;
}
