package com.cyou.fz.pay.business.constant;

/**
 * app版本信息常量.
 * 
 */
public class AppVersionConstant {

	/**
	 * 平台信息 安卓
	 */
	public final static Long PLATFORM_ANDROID = 0L;

	/**
	 * 平台信息 苹果
	 */
	public final static Long PLATFORM_ISO = 1L;

	/**
	 * 发布
	 */
	public final static Long RELEASE = 0L;
	/**
	 * 未发布
	 */
	public final static Long UNRELEASE = 1L;
	
}
