package com.cyou.fz.pay.business.constant;

/**
 * 用户管理常量.
 * 
 * @author Dipin
 * @date 2013-5-16 下午3:01:47
 */
public class UserConstant {
	/**
	 * 后台用户初始化密码.
	 */
	public final static String DEFAULT_PASSWORD = "123456";

	/**
	 * 用户状态——启用.
	 */
	public final static String STATUS_ENABLE = "0";
	/**
	 * 用户状态——停用.
	 */
	public final static String STATUS_DISABLE = "1";
	/**
	 * 用户状态——已加入球队.
	 */
	public final static Integer HAS_TEAM = 1;
	/**
	 * 用户状态——未加入球队.
	 */
	public final static Integer NOT_HAS_TEAM = 0;
	/**
	 * 消息状态——已读.
	 */
	public final static Integer HAS_READ = 1;
	/**
	 * 消息状态——未读.
	 */
	public final static Integer  UNREAD= 0;
	
}
