package com.cyou.fz.pay.business.constant;

/**
 * 城市信息常量.
 * 
 */
public class CityConstant {

	/**
	 * 信息前台显示
	 */
	public final static Long SHOW = 1L;
	/**
	 * 信息前台不显示
	 */
	public final static Long UNSHOW = 0L;

}
