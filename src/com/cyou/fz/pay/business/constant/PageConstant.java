package com.cyou.fz.pay.business.constant;

/**
 * 页面的常用量.
 * 
 * @author qingwu
 * @date 2013-04-19 下午02:00:00
 */
public class PageConstant {
	private PageConstant() {

	}
	
	/**
	 * 登录页.
	 */
	public static final String FRONT_LOGIN = "/front/login";
}
