package com.cyou.fz.pay.business.constant;

/**
 * session中key常量
 * 
 * @Company : cyou
 * @author yangz
 * @date 2012-10-8 下午04:21:11
 */
public final class SessionKeyConstant {
	private SessionKeyConstant() {
	}

	/**
	 * 当前登录用户账号.
	 */
	public final static String ADMIN_LOGIN_USER_ACCOUNT = "adminLoginUserAccount";
	/**
	 * 当前登录用户.
	 */
	public final static String ADMIN_LOGIN_USER = "adminLoginUser";

}
