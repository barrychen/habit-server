package com.cyou.fz.pay.business.constant;

/**
 * 公共信息常量.
 * 
 */
public class PublicConstant {

	/**
	 * 信息前台显示
	 */
	public final static Long SHOW = 0L;
	/**
	 * 信息前台不显示
	 */
	public final static Long UNSHOW = 1L;
	
	
	/**
	 * 删除
	 */
	public final static Long UNDELETE = 0L;
	/**
	 * 未删除
	 */
	public final static Long DELETE = 1L;
	
	/**
	 * 后台地址
	 */
	public final static String SERVERADDRESS = "http://192.168.1.10:8080/baso2o/";
}
