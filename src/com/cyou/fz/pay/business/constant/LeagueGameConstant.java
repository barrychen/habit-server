package com.cyou.fz.pay.business.constant;

/**
 * 联赛信息常量.
 * 
 */
public class LeagueGameConstant {

	/**
	 * 比赛
	 */
	public final static Long ON_GAME = 0L;
	/**
	 * 淘汰
	 */
	public final static Long LOST_GAME = 1L;
	
	/**
	 * 结束
	 */
	public final static Long END_GAME = 2L;
	

	/**
	 * 平局
	 */
	public final static Long DRAW_GAME = 3L;
	
}
