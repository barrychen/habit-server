package com.cyou.fz.pay.business.dao;

import java.util.List;
import java.util.Map;

import com.cyou.fz.common.mybatis.MyBatisRepository;
import com.cyou.fz.pay.business.po.FriendPO;
import com.cyou.fz.pay.business.po.HabitPO;

/**
 * Dao层接口.
 * 
 */
@MyBatisRepository
public interface IFriendDao {
	/**
	 * 新增.
	 * 
	 * @param para
	 */
	void insert(FriendPO para);

	/**
	 * 删除.
	 * 
	 * @param id
	 */
	void delete(Integer id);

	/**
	 * 查找单条记录.
	 * 
	 */
	FriendPO selectOne(Integer id);

	/**
	 * 修改.
	 * 
	 */
	void update(FriendPO para);

	int count(Map<String, Object> para);

	List<FriendPO> findByPage(Map<String, Object> para);

	List<FriendPO> getFriendList(Map<String, Object> para);

	int getFriendPOListCount(Map<String, Object> para);
	
	List<FriendPO> findByExample(Map<String, Object> para);
}
