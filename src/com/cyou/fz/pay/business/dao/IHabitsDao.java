package com.cyou.fz.pay.business.dao;

import java.util.List;
import java.util.Map;

import com.cyou.fz.common.mybatis.MyBatisRepository;
import com.cyou.fz.pay.business.po.HabitPO;

/**
 * Dao层接口.
 * 
 */
@MyBatisRepository
public interface IHabitsDao {
	/**
	 * 新增.
	 * 
	 * @param para
	 */
	void insert(HabitPO para);

	/**
	 * 删除.
	 * 
	 * @param id
	 */
	void delete(Integer id);

	/**
	 * 查找单条记录.
	 * 
	 */
	HabitPO selectOne(Integer id);

	/**
	 * 修改.
	 * 
	 */
	void update(HabitPO para);

	int count(Map<String, Object> para);

	List<HabitPO> findByPage(Map<String, Object> para);

	List<HabitPO> getHabitList(Map<String, Object> para);

	int getHabitListCount(Map<String, Object> para);
	
	List<HabitPO> findByExample(Map<String, Object> para);
}
