package com.cyou.fz.pay.business.dao;

import java.util.List;
import java.util.Map;

import com.cyou.fz.common.mybatis.MyBatisRepository;
import com.cyou.fz.pay.business.po.CheckinPO;
import com.cyou.fz.pay.business.po.HabitPO;

/**
 * Dao层接口.
 * 
 */
@MyBatisRepository
public interface ICheckinDao {
	/**
	 * 新增.
	 * 
	 * @param para
	 */
	void insert(CheckinPO para);

	/**
	 * 删除.
	 * 
	 * @param id
	 */
	void delete(Integer id);

	/**
	 * 查找单条记录.
	 * 
	 */
	CheckinPO selectOne(Integer id);

	/**
	 * 修改.
	 * 
	 */
	void update(CheckinPO para);

	int count(Map<String, Object> para);

	List<CheckinPO> findByPage(Map<String, Object> para);

	List<CheckinPO> getCheckinList(Map<String, Object> para);

	int getCheckinListCount(Map<String, Object> para);
	
	List<CheckinPO> findByExample(Map<String, Object> para);
}
