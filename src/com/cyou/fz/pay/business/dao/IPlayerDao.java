package com.cyou.fz.pay.business.dao;

import java.util.List;
import java.util.Map;

import com.cyou.fz.common.mybatis.MyBatisRepository;
import com.cyou.fz.pay.business.po.PlayerPO;

/**
 * Dao层接口.
 * 
 */
@MyBatisRepository
public interface IPlayerDao {
	/**
	 * 新增.
	 * 
	 * @param para
	 */
	void insert(PlayerPO para);

	/**
	 * 删除.
	 * 
	 * @param id
	 */
	void delete(Integer id);

	/**
	 * 查找单条记录.
	 * 
	 */
	PlayerPO selectOne(Integer id);

	/**
	 * 修改.
	 * 
	 */
	void update(PlayerPO para);

	int count(Map<String, Object> para);

	List<PlayerPO> findByPage(Map<String, Object> para);
	
	/**
	 * 实例查询
	 * 
	 */
	List<PlayerPO> findByExample(Map<String, Object> para);
	
}
