package com.cyou.fz.pay.business.po;

/**
 * 
 * 类说明 : .
 * 
 * <pre>
 * 修改记录：
 * 修改日期　　　修改人　　　修改原因
 * </pre>
 */
public class PlayerPO implements java.io.Serializable {
	/**
	 * 序列化版本.
	 */
	private static final long serialVersionUID = 1L;

	// Fields
	/**
	 * 用户id.
	 */
	private Long id;
	
	/**
	 * 用户email.
	 */
	private String email;
	/**
	 * 用户名.
	 */
	private String username;
	/**
	 * 用户密码.
	 */
	private String password;
	/**
	 * 用户状态.
	 */
	private Integer status;
	
	// Constructors

	/** default constructor */
	public PlayerPO() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}