package com.cyou.fz.pay.business.po;

import java.util.Date;

/**
 * 
 * 类说明 : .
 * 
 * <pre>
 * 修改记录：
 * 修改日期　　　修改人　　　修改原因
 * </pre>
 */
public class FriendPO implements java.io.Serializable {
	/**
	 * 序列化版本.
	 */
	private static final long serialVersionUID = 1L;

	// Fields
	/**
	 * id.
	 */
	private Integer id;
	
	/**
	 * userA.
	 */
	private Integer userA;
	
	/**
	 * userB.
	 */
	private Integer userB;
	
	// Fields
	/**
	 * status.
	 */
	private Integer status;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserA() {
		return userA;
	}

	public void setUserA(Integer userA) {
		this.userA = userA;
	}

	public Integer getUserB() {
		return userB;
	}

	public void setUserB(Integer userB) {
		this.userB = userB;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
}