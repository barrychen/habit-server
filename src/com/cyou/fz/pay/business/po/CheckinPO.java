package com.cyou.fz.pay.business.po;

import java.util.Date;

public class CheckinPO {
	private int id;
	private int userId;
	private int habitId;
	private String content;
	private Date insertedTime;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getHabitId() {
		return habitId;
	}
	public void setHabitId(int habitId) {
		this.habitId = habitId;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getInsertedTime() {
		return insertedTime;
	}
	public void setInsertedTime(Date insertedTime) {
		this.insertedTime = insertedTime;
	}

}
