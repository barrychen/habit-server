package com.cyou.fz.pay.business.po;

import java.util.Date;

/**
 * 
 * 类说明 : .
 * 
 * <pre>
 * 修改记录：
 * 修改日期　　　修改人　　　修改原因
 * </pre>
 */
public class HabitPO implements java.io.Serializable {
	/**
	 * 序列化版本.
	 */
	private static final long serialVersionUID = 1L;

	// Fields
	/**
	 * id.
	 */
	private Integer id;
	
	/**
	 * id.
	 */
	private Integer userId;
	
	/**
	 * 内容.
	 */
	private String content;
	
	// Fields
	/**
	 * 进度.
	 */
	private Integer progress;
	
	/**
	 * id.
	 */
	private Integer friendId;
	
	/**
	 * id.
	 */
	private Integer friendProgress;

	/**
	 * 最后更新时间.
	 */
	private Date lastUpdated;
	
	/**
	 * 朋友最后更新时间.
	 */
	private Date friendLastUpdated;
	
	/**
	 * id.
	 */
	private Integer approved;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getProgress() {
		return progress;
	}

	public void setProgress(Integer progress) {
		this.progress = progress;
	}

	public Integer getFriendId() {
		return friendId;
	}

	public void setFriendId(Integer friendId) {
		this.friendId = friendId;
	}

	public Integer getFriendProgress() {
		return friendProgress;
	}

	public void setFriendProgress(Integer friendProgress) {
		this.friendProgress = friendProgress;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public Date getFriendLastUpdated() {
		return friendLastUpdated;
	}

	public void setFriendLastUpdated(Date friendLastUpdated) {
		this.friendLastUpdated = friendLastUpdated;
	}

	public Integer getApproved() {
		return approved;
	}

	public void setApproved(Integer approved) {
		this.approved = approved;
	}
	
}