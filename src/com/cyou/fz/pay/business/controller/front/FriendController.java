package com.cyou.fz.pay.business.controller.front;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cyou.fz.common.ajax.Response;
import com.cyou.fz.common.ajax.ResponseFactory;
import com.cyou.fz.common.utils.ObjectUtil;
import com.cyou.fz.pay.business.po.FriendPO;
import com.cyou.fz.pay.business.po.PlayerPO;
import com.cyou.fz.pay.business.service.CheckinService;
import com.cyou.fz.pay.business.service.FriendService;
import com.cyou.fz.pay.business.service.PlayerService;
import com.cyou.fz.pay.business.vo.FriendVO;
import com.cyou.fz.pay.page.ListPage;

/**
 * 
 */
@Controller
@RequestMapping("/friend")
public class FriendController {
	@Autowired
	private FriendService friendService;

	private static final Logger LOGGER = LoggerFactory
			.getLogger(FriendController.class);

	@Autowired
	private PlayerService playerService;


	/**
	 * 添加好友
	 */
	@ResponseBody
	@RequestMapping("/makerequest")
	public Response<?> add(Integer userA, Integer userB, 
			HttpServletResponse httpresponse) {
		FriendPO po = new FriendPO();
		if (!ObjectUtil.isEmpty(userA)) {
			po.setUserA(userA);
		}
		if (!ObjectUtil.isEmpty(userB)) {
			po.setUserB(userB);
		}
		po.setStatus(0);

		Response<FriendPO> response = new Response<FriendPO>();
		try {
			//插入数据
			friendService.add(po);
			response.setResult(Response.RESULT_SUCCESS);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			response = ResponseFactory.getDefaultFailureResponse("新增失败");
		}

		httpresponse.setHeader("Access-Control-Allow-Origin", "*");
		return response;
	}

	
	/**
	 * 更新friend
	 */
	@ResponseBody
	@RequestMapping("/respondtorequest")
	public Response<?> respondtorequest(Integer acceptOrDecline,Integer id,Integer userA) {
		Response<Boolean> response = new Response<Boolean>();;
		try {
			FriendPO friend=friendService.findOne(id);
			//如果同意则更新，不同意则删除
			if(acceptOrDecline==1){
				friend.setStatus(acceptOrDecline);
				friendService.update(friend);
			}else{
				friendService.delete(id);
			}
			response.setResult(Response.RESULT_SUCCESS);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			response.setResult(Response.RESULT_FAILURE);
			response = ResponseFactory.getDefaultFailureResponse("更新失败");
		}
		return response;
	}
	
	/**
	 * friend列表 
	 */
	@ResponseBody
	@RequestMapping("/findfriend")
	public Response<List<PlayerPO>> findfriend(String username, HttpServletResponse httpresponse) {
		Response<List<PlayerPO>> response = new Response<List<PlayerPO>>();
		try {
			List<PlayerPO> list = playerService.findByUserName(username);
			response.setData(list);
			response.setResult(Response.RESULT_SUCCESS);
			httpresponse.setHeader("Access-Control-Allow-Origin", "*");
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			response = ResponseFactory.getDefaultFailureResponse("查询失败");
		}
		return response;
	}

	/**
	 * 取得所有friend请求列表 
	 */
	@ResponseBody
	@RequestMapping("/loadfriendrequests")
	public Response<List<FriendVO>> loadfriendrequests(Integer userB, HttpServletResponse httpresponse) {
		Response<List<FriendVO>> response = new Response<List<FriendVO>>();
		try {
			List<FriendVO> list = friendService.getFriendRequestVOList(userB);
			response.setData(list);
			response.setResult(Response.RESULT_SUCCESS);
			httpresponse.setHeader("Access-Control-Allow-Origin", "*");
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			response = ResponseFactory.getDefaultFailureResponse("查询失败");
		}
		return response;
	}
	
	/**
	 * friend列表 
	 */
	@ResponseBody
	@RequestMapping("/loadfriends")
	public Response<List<FriendVO>> loadfriends(Integer userId, HttpServletResponse httpresponse) {
		Response<List<FriendVO>> response = new Response<List<FriendVO>>();
		try {
			List<FriendVO> list = friendService.getFriendVOList(userId);
			response.setData(list);
			response.setResult(Response.RESULT_SUCCESS);
			httpresponse.setHeader("Access-Control-Allow-Origin", "*");
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			response = ResponseFactory.getDefaultFailureResponse("查询失败");
		}
		return response;
	}

}
