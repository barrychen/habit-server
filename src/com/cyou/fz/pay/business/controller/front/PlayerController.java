package com.cyou.fz.pay.business.controller.front;

import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cyou.fz.common.ajax.Response;
import com.cyou.fz.common.ajax.ResponseFactory;
import com.cyou.fz.pay.business.po.PlayerPO;
import com.cyou.fz.pay.business.service.PlayerService;

/**
 * 
 */
@Controller
@RequestMapping("/player")
public class PlayerController {
	@Autowired
	private PlayerService playerService;
	private static final Logger LOGGER = LoggerFactory
			.getLogger(PlayerController.class);

	/**
	 * 注册用户
	 */
	@ResponseBody
	@RequestMapping("/reg")
	public Response<?> add(String email,String userName, String password,HttpServletResponse httpresponse) {
		Response<PlayerPO> response = null;
		if(StringUtils.isNotEmpty(email) && StringUtils.isNotEmpty(password)){
			PlayerPO po = new PlayerPO();
			
			if (StringUtils.isNotEmpty(email)) {
				po.setEmail(email);
			}
			if (StringUtils.isNotEmpty(password)) {
				po.setPassword(password);
			}
			try {
				po.setUsername(userName);
				po.setStatus(1);
				playerService.add(po);
				response = ResponseFactory.getDefaultSuccessResponse();
				response.setData(playerService.findByEmail(email).get(0));
				httpresponse.setHeader("Access-Control-Allow-Origin", "*");
			} catch (Exception e) {
				LOGGER.error(e.getMessage());
				response = ResponseFactory.getDefaultFailureResponse("新增失败");
			}
			
		}else{
			response = ResponseFactory.getDefaultFailureResponse("输入信息有误！");
			response.setMessage("输入信息有误！");
		}
		return response;
	}
	
	/**
	 * 更新用户
	 */
	@ResponseBody
	@RequestMapping("/update")
	public Response<?> update(PlayerPO po) {
		Response<Boolean> response = null;
		try {
			playerService.update(po);
			response = ResponseFactory.getDefaultSuccessResponse();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			response = ResponseFactory.getDefaultFailureResponse("更新失败");
		}
		return response;
	}
	

	/**
	 * 禁用用户
	 */
	@ResponseBody
	@RequestMapping("/delete")
	public Response<?> delete(Integer id) {
		Response<Boolean> response = null;
		try {
			playerService.delete(id);
			response = ResponseFactory.getDefaultSuccessResponse();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			response = ResponseFactory.getDefaultFailureResponse("删除失败");
		}
		return response;
	}

	/**
	 * 根据id取用户详细信息
	 */
	@ResponseBody
	@RequestMapping("/get")
	public Response<PlayerPO> get(Integer id,HttpServletResponse httpresponse) {
		Response<PlayerPO> response = new Response<PlayerPO>();
		try {
			PlayerPO po = playerService.findOne(id);
			response.setData(po);
			response.setResult(Response.RESULT_SUCCESS);
			httpresponse.setHeader("Access-Control-Allow-Origin", "*");
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			response = ResponseFactory.getDefaultFailureResponse("查询失败");
		}
		return response;
	}


	/**
	 * 根据email判断用户是否存在
	 */
	@ResponseBody
	@RequestMapping("/checkExist")
	public Response<Integer> checkExist(String email,HttpServletResponse httpresponse) {
		Response<Integer> response = new Response<Integer>();
		try {
			int count=playerService.countByEmail(email);
			if(count>0){
				response.setData(new Integer(1));
				response = ResponseFactory.getDefaultFailureResponse("查询失败");
				//response.setResult(Response.RESULT_FAILURE);
			}else{
				//response.setResult(Response.RESULT_SUCCESS); 
				response.setData(new Integer(0));
				response = ResponseFactory.getDefaultSuccessResponse();
			}
			httpresponse.setHeader("Access-Control-Allow-Origin", "*");
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			response = ResponseFactory.getDefaultFailureResponse("查询失败");
		}
		return response;
	}
	
	/**
	 * 根据email进行登录 
	 */
	@ResponseBody
	@RequestMapping("/login")
	public Response<PlayerPO> login(String email,String password,HttpServletResponse httpresponse) {
		Response<PlayerPO> response = new Response<PlayerPO>();
		try {
			List<PlayerPO> list=playerService.findByEmail(email);
			if(list.size()>0){
				PlayerPO loginPlayer=list.get(0);
				if(loginPlayer.getPassword().equals(password)){
					response = ResponseFactory.getDefaultSuccessResponse();
					response.setData(loginPlayer);
				}else{
					response = ResponseFactory.getDefaultFailureResponse("用户名或密码错误!");
				}
			}else{
				response = ResponseFactory.getDefaultFailureResponse("用户名或密码错误!");
			}
			httpresponse.setHeader("Access-Control-Allow-Origin", "*");
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			response = ResponseFactory.getDefaultFailureResponse("查询失败");
		}
		return response;
	}	

}
