package com.cyou.fz.pay.business.controller.front;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cyou.fz.common.ajax.Response;
import com.cyou.fz.common.ajax.ResponseFactory;
import com.cyou.fz.common.utils.ObjectUtil;
import com.cyou.fz.pay.business.po.CheckinPO;
import com.cyou.fz.pay.business.po.HabitPO;
import com.cyou.fz.pay.business.service.CheckinService;
import com.cyou.fz.pay.business.service.HabitsService;
import com.cyou.fz.pay.business.vo.CheckinVO;
import com.cyou.fz.pay.business.vo.HabitVO;
import com.cyou.fz.pay.page.ListPage;

/**
 * 
 */
@Controller
@RequestMapping("/habit")
public class HabitController {
	@Autowired
	private HabitsService habitsService;

	private static final Logger LOGGER = LoggerFactory
			.getLogger(HabitController.class);

	@Autowired
	private CheckinService checkinService;


	/**
	 * 创建habit
	 */
	@ResponseBody
	@RequestMapping("/add")
	public Response<?> add(Integer userId, String content, 
			Integer friendId, 
			HttpServletResponse httpresponse) {
		HabitPO po = new HabitPO();
		if (!ObjectUtil.isEmpty(userId)) {
			po.setUserId(userId);
		}
		if (StringUtils.isNotEmpty(content)) {
			po.setContent(content);
		}
		po.setProgress(1);
		if (!ObjectUtil.isEmpty(friendId)) {
			po.setFriendId(friendId);
			po.setApproved(0);
		}else{
			po.setFriendId(userId);
			po.setApproved(1);
		}

		Response<HabitPO> response = new Response<HabitPO>();
		try {
			//插入数据
			habitsService.add(po);
			//取得带id的habit
			Map<String, Object> para=new HashMap<>();
			para.put("content", content);
			HabitPO data=habitsService.findList(para).get(0);
			//设置返回值
			response.setData(data);
			response.setResult(Response.RESULT_SUCCESS);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			response = ResponseFactory.getDefaultFailureResponse("新增失败");
		}

		httpresponse.setHeader("Access-Control-Allow-Origin", "*");
		return response;
	}

	
	/**
	 * 更新habit
	 */
	@ResponseBody
	@RequestMapping("/update")
	public Response<?> update(HabitPO po) {
		Response<Boolean> response = null;
		try {
			habitsService.update(po);
			response = ResponseFactory.getDefaultSuccessResponse();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			response = ResponseFactory.getDefaultFailureResponse("更新失败");
		}
		return response;
	}

	
	/**
	 * update checkin
	 */
	@ResponseBody
	@RequestMapping("/checkin/comment")
	public Response<CheckinPO> checkinComment(Integer id,Integer userId,String content, HttpServletResponse httpresponse) {
		Response<CheckinPO> response = new Response<CheckinPO>();
		try {
			//取得checkin
			CheckinPO checkinPO = checkinService.findOne(id);
			//更新数据里的content字段
			checkinPO.setContent(content);
			checkinService.update(checkinPO);
			
			//更新habits表的content和progress字段
			HabitPO habitPO=habitsService.findOne(checkinPO.getHabitId());
			habitPO.setContent(content);
			habitPO.setProgress(habitPO.getProgress()+1);
			habitsService.update(habitPO);
			
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			response = ResponseFactory.getDefaultFailureResponse("查询失败");
		}

		httpresponse.setHeader("Access-Control-Allow-Origin", "*");
		return response;
	}
	
	/**
	 * check in habit
	 */
	@ResponseBody
	@RequestMapping("/checkin")
	public Response<CheckinPO> checkin(Integer id,Integer userId, HttpServletResponse httpresponse) {
		Response<CheckinPO> response = new Response<CheckinPO>();
		try {
			//判断今天是否已经checkin过
			HabitPO po = habitsService.findOne(id);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		    String lastUpdate = sdf.format(po.getLastUpdated());
		    String now = sdf.format(new Date());
		    //如果今天没check in过
		    if(!now.equals(lastUpdate)){
		    	//插入数据
		    	CheckinPO checkin=new CheckinPO();
		    	checkin.setHabitId(po.getId());
		    	checkin.setUserId(po.getUserId());
		    	checkinService.add(checkin);
		    	//取得带id的checkin
				Map<String, Object> para=new HashMap<>();
				para.put("habitId", id);
				para.put("userId", po.getUserId());
				CheckinPO checkinPo=checkinService.findList(para).get(0);
				
				//更新habits
				//(a)  userId等于当前请求用户id， 则更新habits表的progress和lastUpdated
				if(po.getUserId()==userId.intValue()){
					po.setProgress(po.getProgress()+1);
					po.setLastUpdated(new Date());
				}
				//(b)  friendId等于当前请求用户id， 则更新friendProgress和friendLastUpdated。 
				else if(po.getFriendId()==userId.intValue()){
					po.setFriendProgress(po.getFriendProgress()+1);
					po.setFriendLastUpdated(new Date());
				}
				habitsService.update(po);
				//设置返回值
		    	response.setResult(Response.RESULT_SUCCESS);
		    	response.setData(checkinPo);
		    }else{
				response.setResult(Response.RESULT_FAILURE);
		    }
			
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			response = ResponseFactory.getDefaultFailureResponse("查询失败");
		}

		httpresponse.setHeader("Access-Control-Allow-Origin", "*");
		return response;
	}
	
	/**
	 * 根据id取habit详细信息
	 */
	@ResponseBody
	@RequestMapping("/detail")
	public Response<HabitPO> detail(Integer pactId, HttpServletResponse httpresponse) {
		Response<HabitPO> response = new Response<HabitPO>();
		try {
			HabitPO po = habitsService.findOne(pactId);
			response.setData(po);
			response.setResult(Response.RESULT_SUCCESS);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			response = ResponseFactory.getDefaultFailureResponse("查询失败");
		}

		httpresponse.setHeader("Access-Control-Allow-Origin", "*");
		return response;
	}

	/**
	 * habit列表 搜索habit
	 */
	@ResponseBody
	@RequestMapping("/getHabitVOList")
	public Response<ListPage<HabitVO>> getHabitVOList(int pageNo, int pageSize,
			Integer userId, HttpServletResponse httpresponse) {
		Response<ListPage<HabitVO>> response = new Response<ListPage<HabitVO>>();
		try {
			ListPage<HabitVO> list = habitsService.getHabitVOList(pageNo, pageSize,userId);
			response.setData(list);
			response.setResult(Response.RESULT_SUCCESS);
			httpresponse.setHeader("Access-Control-Allow-Origin", "*");
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			response = ResponseFactory.getDefaultFailureResponse("查询失败");
		}
		return response;
	}

	/**
	 * habit列表 搜索habit
	 */
	@ResponseBody
	@RequestMapping("/loadhabitrequests")
	public Response<List<HabitVO>> loadhabitrequests(Integer userId, HttpServletResponse httpresponse) {
		Response<List<HabitVO>> response = new Response<List<HabitVO>>();
		try {
			List<HabitVO> list = habitsService.loadhabitrequests(userId);
			response.setData(list);
			response.setResult(Response.RESULT_SUCCESS);
			httpresponse.setHeader("Access-Control-Allow-Origin", "*");
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			response = ResponseFactory.getDefaultFailureResponse("查询失败");
		}
		return response;
	}
	
	/**
	 * 响应好友habit邀请
	 */
	@ResponseBody
	@RequestMapping("/respondtohabit")
	public Response<HabitPO> respondtohabit(Integer id,Integer approved, HttpServletResponse httpresponse) {
		Response<HabitPO> response = new Response<HabitPO>();
		try {
			HabitPO po = habitsService.findOne(id);
			if(approved==0){
				po.setApproved(1);
				po.setFriendId(po.getUserId());
			}else{
				po.setApproved(1);
				po.setFriendProgress(1);
			}
			habitsService.update(po);
			response.setData(po);
			response.setResult(Response.RESULT_SUCCESS);
			httpresponse.setHeader("Access-Control-Allow-Origin", "*");
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			response = ResponseFactory.getDefaultFailureResponse("查询失败");
		}
		return response;
	}
	
	/**
	 * 响应好友habit邀请
	 */
	@ResponseBody
	@RequestMapping("/checkinList")
	public Response<List<CheckinVO>> checkinList(Integer habitId,Integer userId, HttpServletResponse httpresponse) {
		Response<List<CheckinVO>> response = new Response<List<CheckinVO>>();
		try {
			List<CheckinVO> voList = checkinService.getCheckinVOList(userId,habitId);
			response.setData(voList);
			response.setResult(Response.RESULT_SUCCESS);
			httpresponse.setHeader("Access-Control-Allow-Origin", "*");
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			response = ResponseFactory.getDefaultFailureResponse("查询失败");
		}
		return response;
	}
	
}
