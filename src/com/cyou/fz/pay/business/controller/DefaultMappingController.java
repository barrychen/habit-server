package com.cyou.fz.pay.business.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 默认地址匹配规则.
 * 
 * @author yangz
 * @date 2013-4-15 下午2:16:41
 */
@Controller
public class DefaultMappingController {

	/**
	 * 默认匹配所有地址.
	 * @return
	 * @author yangz
	 * @date 2013-4-15 下午2:16:00
	 */
	@RequestMapping("/**/*")
	public ModelAndView defaultMapping() {
		ModelAndView view = new ModelAndView();
		return view;
	}
	
}
