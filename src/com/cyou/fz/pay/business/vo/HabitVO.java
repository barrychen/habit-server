package com.cyou.fz.pay.business.vo;

import com.cyou.fz.pay.business.po.HabitPO;

public class HabitVO extends HabitPO{
	private String progressStr;
	private String userName;
	private String friendName;
	public String getProgressStr() {
		if(this.getProgress()<=7){
			return "第一阶段";
		}
		else if(this.getProgress()<21 && 7<this.getProgress()){
			return "第二阶段";
		}
		else if(21<=this.getProgress()){
			return "第三阶段";
		}else{
			return progressStr;
		}
	}

	public void setProgressStr(String progressStr) {
		this.progressStr = progressStr;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFriendName() {
		return friendName;
	}

	public void setFriendName(String friendName) {
		this.friendName = friendName;
	}
}
