package com.cyou.fz.pay.business.vo;

import java.text.SimpleDateFormat;

import com.cyou.fz.pay.business.po.CheckinPO;

public class CheckinVO extends CheckinPO{
	private String dateStr;

	public String getDateStr() {
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		return sdf.format(this.getInsertedTime());
	}

	public void setDateStr(String dateStr) {
		this.dateStr = dateStr;
	}
	
}
