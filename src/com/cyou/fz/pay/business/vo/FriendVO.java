package com.cyou.fz.pay.business.vo;

import com.cyou.fz.pay.business.po.FriendPO;
import com.cyou.fz.pay.business.po.PlayerPO;

public class FriendVO extends FriendPO{
	private PlayerPO userABean;
	private PlayerPO userBBean;
	public PlayerPO getUserABean() {
		return userABean;
	}
	public void setUserABean(PlayerPO userABean) {
		this.userABean = userABean;
	}
	public PlayerPO getUserBBean() {
		return userBBean;
	}
	public void setUserBBean(PlayerPO userBBean) {
		this.userBBean = userBBean;
	}
}
