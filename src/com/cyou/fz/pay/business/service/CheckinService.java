package com.cyou.fz.pay.business.service;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cyou.fz.pay.business.dao.ICheckinDao;
import com.cyou.fz.pay.business.dao.IHabitsDao;
import com.cyou.fz.pay.business.po.CheckinPO;
import com.cyou.fz.pay.business.po.HabitPO;
import com.cyou.fz.pay.business.vo.CheckinVO;
import com.cyou.fz.pay.business.vo.HabitVO;
import com.cyou.fz.pay.page.ListPage;

/**
 * 
 */
@Service
public class CheckinService {
	private static Logger log = Logger.getLogger(CheckinService.class);

	@Autowired
	private ICheckinDao checkinDao;

	/**
	 * 新增 .
	 */
	public void add(CheckinPO checkinPO) {
		checkinPO.setInsertedTime(new Date());
		checkinDao.insert(checkinPO);
	}

	public void update(CheckinPO checkinPO) {
		checkinDao.update(checkinPO);
	}

	public void delete(Integer id) {
		checkinDao.delete(id);
	}

	public List<CheckinPO> findList(Map para) {
		List<CheckinPO> poList = checkinDao.findByExample(para);
		return poList;
	}
	
	public CheckinPO findOne(Integer id) {
		CheckinPO po = checkinDao.selectOne(id);
		return po;
	}
	
	public List<CheckinVO> getCheckinVOList(Integer userId, Integer habitId) {
		Map<String, Object> para = new HashMap<String, Object>();
		para.put("pageNo", 0);
		para.put("pageSize", 100);
		para.put("userId", userId);
		para.put("habitId", habitId);
		List<CheckinPO> dataList = checkinDao.getCheckinList(para);
		List<CheckinVO> voList=new ArrayList<>();
		for(int i=0;i<dataList.size();i++){
			CheckinPO po=dataList.get(i);
			CheckinVO vo=new CheckinVO();
			try {
				BeanUtils.copyProperties(vo, po);
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			voList.add(vo);
		}
		return voList;
	}
}
