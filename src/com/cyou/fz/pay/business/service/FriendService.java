package com.cyou.fz.pay.business.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cyou.fz.pay.business.dao.IFriendDao;
import com.cyou.fz.pay.business.po.FriendPO;
import com.cyou.fz.pay.business.vo.FriendVO;
import com.cyou.fz.pay.page.ListPage;

/**
 * 
 */
@Service
public class FriendService {

	@Autowired
	private IFriendDao friendDao;

	@Autowired
	private PlayerService playerService;
	/**
	 * 新增 .
	 */
	public void add(FriendPO habitPO) {
		friendDao.insert(habitPO);
	}

	public void update(FriendPO habitPO) {
		friendDao.update(habitPO);
	}

	public void delete(Integer id) {
		friendDao.delete(id);
	}

	public List<FriendPO> findList(Map para) {
		List<FriendPO> poList = friendDao.findByExample(para);
		return poList;
	}
	
	public FriendPO findOne(Integer id) {
		FriendPO po = friendDao.selectOne(id);
		return po;
	}
	
	/**
	 * 取得好友请求列表
	 * @param pageNo
	 * @param pageSize
	 * @param userId
	 * @return
	 */
	public List<FriendVO> getFriendRequestVOList( Integer userId) {
		Map<String, Object> para = new HashMap<String, Object>();
		para.put("pageNo", 0);
		para.put("pageSize", 100);
		para.put("userB", userId);
		para.put("status", 0);
		List<FriendPO> dataList = friendDao.getFriendList(para);
		List<FriendVO> voList=new ArrayList<>();
		for(int i=0;i<dataList.size();i++){
			FriendPO po=dataList.get(i);
			FriendVO vo=new FriendVO();
			try {
				BeanUtils.copyProperties(po,vo);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			vo.setUserABean(playerService.findOne(vo.getUserA()));
			vo.setUserBBean(playerService.findOne(vo.getUserB()));
			voList.add(vo);
		}
		return voList;
	}
	
	/**
	 * 取得好友列表
	 * @param pageNo
	 * @param pageSize
	 * @param userId
	 * @return
	 */
	public List<FriendVO> getFriendVOList( Integer userId) {
		Map<String, Object> para = new HashMap<String, Object>();
		para.put("pageNo", 0);
		para.put("pageSize", 100);
		para.put("userA", userId);
		para.put("status", 1);
		List<FriendPO> dataList = friendDao.getFriendList(para);
		
		Map<String, Object> para2 = new HashMap<String, Object>();
		para2.put("pageNo", 0);
		para2.put("pageSize", 100);
		para2.put("userB", userId);
		para2.put("status", 1);
		List<FriendPO> dataList2 = friendDao.getFriendList(para2);
		
		List<FriendVO> voList=new ArrayList<>();
		dataList.addAll(dataList2);
		for(int i=0;i<dataList.size();i++){
			FriendPO po=dataList.get(i);
			FriendVO vo=new FriendVO();
			try {
				BeanUtils.copyProperties(po,vo);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			vo.setUserABean(playerService.findOne(vo.getUserA()));
			vo.setUserBBean(playerService.findOne(vo.getUserB()));
			voList.add(vo);
		}
		return voList;
	}
}
