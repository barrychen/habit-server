package com.cyou.fz.pay.business.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cyou.fz.pay.business.dao.IPlayerDao;
import com.cyou.fz.pay.business.po.PlayerPO;

/**
 * 
 */
@Service
public class PlayerService {
	private static Logger log = Logger.getLogger(PlayerService.class);

	@Autowired
	private IPlayerDao playerDao;


	/**
	 * 新增 .
	 */
	public void add(PlayerPO playerPO) {
		playerDao.insert(playerPO);

	}

	public void update(PlayerPO playerPO) {
		playerDao.update(playerPO);
	}

	public void delete(Integer id) {
		playerDao.delete(id);
	}

	public PlayerPO findOne(Integer id) {
		PlayerPO po = playerDao.selectOne(id);
		return po;
	}

	public List<PlayerPO> findByUserName(String username) {
		Map<String, Object> para = new HashMap<>();
		para.put("username", username);
		return playerDao.findByExample(para);
	}
	
	/**
	 * 根据email取得用户数据
	 */
	public List<PlayerPO> findByEmail(String email) {
		Map<String, Object> para = new HashMap<>();
		para.put("email", email);
		return playerDao.findByExample(para);
	}

	public int countByEmail(String email) {
		Map<String, Object> para = new HashMap<>();
		para.put("email", email);
		return playerDao.count(para);
	}
}
