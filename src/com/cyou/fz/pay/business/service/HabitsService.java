package com.cyou.fz.pay.business.service;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cyou.fz.pay.business.dao.IHabitsDao;
import com.cyou.fz.pay.business.dao.IPlayerDao;
import com.cyou.fz.pay.business.po.HabitPO;
import com.cyou.fz.pay.business.vo.HabitVO;
import com.cyou.fz.pay.page.ListPage;

/**
 * 
 */
@Service
public class HabitsService {
	private static Logger log = Logger.getLogger(HabitsService.class);

	@Autowired
	private IHabitsDao habitDao;
	@Autowired
	private IPlayerDao playerDao;
	/**
	 * 新增 .
	 */
	public void add(HabitPO habitPO) {
		habitPO.setLastUpdated(new Date());
		habitDao.insert(habitPO);
	}

	public void update(HabitPO habitPO) {
		habitDao.update(habitPO);
	}

	public void delete(Integer id) {
		habitDao.delete(id);
	}

	public List<HabitPO> findList(Map para) {
		List<HabitPO> poList = habitDao.findByExample(para);
		return poList;
	}
	
	public HabitPO findOne(Integer id) {
		HabitPO po = habitDao.selectOne(id);
		return po;
	}
	
	/**
	 * 取得所有的好友habit邀请
	 * @param userId
	 * @return
	 */
	public List<HabitVO> loadhabitrequests(Integer userId) {
		//条件1：
		//userId==请求里的userId和firendId==请求里的userId， 说明这个habit只是这个用户自己完成， 返回结果无需处理。 
		//userId==请求里的userId和firendId!=请求里的userId， 说明请求里的userId是发起这个habit， 返回结果无需处理。
		//以上可以归结为userId==请求里的userId就无需处理返回结果
		Map<String, Object> para = new HashMap<String, Object>();
		para.put("pageNo", 0);
		para.put("pageSize", 100);
		para.put("friendId", userId);
		para.put("approved", 0);
		List<HabitPO> dataList = habitDao.getHabitList(para);
		List<HabitVO> voList=new ArrayList<>();
		for(int i=0;i<dataList.size();i++){
			HabitPO po=dataList.get(i);
			HabitVO vo=new HabitVO();
			try {
				BeanUtils.copyProperties(po,vo);
				vo.setUserName(playerDao.selectOne(vo.getUserId()).getUsername());
				vo.setFriendName(playerDao.selectOne(vo.getFriendId()).getUsername());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			voList.add(vo);
		}
		
		return voList;
	}
	
	public ListPage<HabitVO> getHabitVOList(int pageNo, int pageSize, Integer userId) {
		ListPage<HabitVO> listPage = new ListPage<HabitVO>();
		listPage.setCurrentPageNo(pageNo);
		listPage.setCurrentPageSize(pageSize);
		//条件1：
		//userId==请求里的userId和firendId==请求里的userId， 说明这个habit只是这个用户自己完成， 返回结果无需处理。 
		//userId==请求里的userId和firendId!=请求里的userId， 说明请求里的userId是发起这个habit， 返回结果无需处理。
		//以上可以归结为userId==请求里的userId就无需处理返回结果
		Map<String, Object> para = new HashMap<String, Object>();
		para.put("pageNo", pageNo);
		para.put("pageSize", pageSize);
		para.put("userId", userId);
		para.put("approved", 1);
		List<HabitPO> dataList = habitDao.getHabitList(para);
		List<HabitVO> voList=new ArrayList<>();
		for(int i=0;i<dataList.size();i++){
			HabitPO po=dataList.get(i);
			HabitVO vo=new HabitVO();
			try {
				BeanUtils.copyProperties(po,vo);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			voList.add(vo);
		}
		//条件2：
		//userId!=请求里的userId和firendId==请求里的userId， 说明这个habit用户是被邀请， 就把返回的结果里userId和friendId对调，progress和friendProgress对调。
		Map<String, Object> para2 = new HashMap<String, Object>();
		para2.put("pageNo", pageNo);
		para2.put("pageSize", pageSize);
		para2.put("friendId", userId);
		para2.put("noUserId", userId);
		para2.put("approved", 1);
		List<HabitPO> dataList2 = habitDao.getHabitList(para2);
		for(int i=0;i<dataList2.size();i++){
			HabitPO po=dataList2.get(i);
			HabitVO vo=new HabitVO();
			try {
				BeanUtils.copyProperties(po,vo);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			voList.add(vo);
		}
		
		listPage.setTotalCount(200);
		listPage.setDataList(voList);
		return listPage;
	}
}
